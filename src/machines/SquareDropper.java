package machines;

import baller.*;
import physXgine.*;

/**
 * Extends Machine. Is a machine that spawns a square when run only if there is
 * nothing in the way.
 *
 * @author Mitchell
 */
public class SquareDropper
        extends Machine
{
    /**
     * Constructs a SquareDropper machine at the given position.
     *
     * @param pos  Vec2 position on FactoryEngine to be used and drawn.
     * @param comp FactoryEngine environment that the machine lives in.
     */
    public SquareDropper( Vec2 pos, FactoryEngine comp )
    {
        super("/Images/SquareDropper.png", pos);
        type = "SquareDropper";
        environment = comp;

        //PolyBox's used to create a concave polygon hitbox.
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-20, -12), new Vec2(20, -20), new Vec2(20,
                                                                            -12)));
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-12, -20), new Vec2(-20, 20), new Vec2(-12,
                                                                            20)));
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(12, -20),
                                   new Vec2(20, -20), new Vec2(12, 20),
                                   new Vec2(20, 20)));
    }

    /**
     * Overrides superclass's orient method so that this machine can not change
     * its orientation.
     *
     * @param radians Double radian value.
     */
    @Override
    public void setOrient( double radians )
    {
    }

    /**
     * Overrides superclass's orient method so that this machine can not change
     * its orientation.
     *
     * @param mat Matrix class.
     */
    @Override
    public void setOrient( Matrix mat )
    {
    }

    /**
     * This run method can be used to spawn a square at this machines position.
     * If spawning a square will result in a collision, then no square is
     * spawned.
     */
    @Override
    public void run()
    {
        //Spawns square at center of tile.
        Square square = new Square(position.x - 7, position.y - 7, 14, 14);
        if( Collision.getCollision(square, environment.getItems()).size() < 1 ){
            //Default blue color.
            square.colour = MainFrame.blue;
            //Square needs to know what Panel its on.
            square.enviroment = environment;
            environment.addItem(square);
        }
    }
}
