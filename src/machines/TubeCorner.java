package machines;

import baller.PolyBox;
import physXgine.*;

/**
 * A machine that serves only as a hit box for a curved quarter pipe block.
 *
 * @author warrm1
 */
public class TubeCorner
        extends Machine
{
    /**
     * Constructs a TubeCorner machine at the given position.
     *
     * @param pos Vec2 position on FactoryEngine to be used and drawn.
     */
    public TubeCorner( Vec2 pos )
    {
        super("/Images/TubeCorner.png", pos);
        type = "TubeCorner";

        //These five PolyBoxs map out the space for a concave polygon. This is required as quick poly collision checking only works with convex polygons.
        //This concave poly shape has to be made up with a composite of convex polygons.
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-12, -20), new Vec2(-20, -9), new Vec2(-12,
                                                                            -9)));
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(-20, -9),
                                   new Vec2(-19, -2), new Vec2(-10, 9),
                                   new Vec2(-5, 9), new Vec2(-12, -9)));
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(-12, -3),
                                   new Vec2(-10, 9), new Vec2(-9, 10), new Vec2(
                                           3, 12)));
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(9, 20),
                                   new Vec2(2, 19), new Vec2(-9, 10), new Vec2(
                                           -9, 5), new Vec2(9, 12)));
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(20, 20),
                                   new Vec2(20, 12), new Vec2(9, 12),
                                   new Vec2(9, 20)));
    }

    /**
     * Can be run as is required by Machine, but whence run it will do nothing.
     */
    @Override
    public void run()
    {
    }
}
