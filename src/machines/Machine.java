package machines;

import baller.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import physXgine.*;
import Images.ImageHolder;

/**
 * Holds the information to produce, show, and run machines. Abstract class
 * requires that extended machines creates at least one Body for ArrayList<Body>
 * composites.
 *
 * @author warrm1
 */
public abstract class Machine
        implements Runnable,
                   FactoryEngineDrawable
{
    public FactoryEngine environment;
    protected Vec2 position;
    public Thread thread;
    BufferedImage img = null;
    public String type;

    //Composites is a list of Bodies used as the machine's hitbox.
    public ArrayList<Body> composites = new ArrayList<>();

    /**
     * Constructs the Machine type and sets the image from the Images Package
     * given the filename.
     *
     * @param fileName String name of the selected Image. E.G.
     *                 "/Images/ClassName.png".
     * @param pos      Vec2 the position of machine.
     */
    public Machine( String fileName, Vec2 pos )
    {
        ImageHolder ih = new ImageHolder(fileName);
        img = ih.getImage();
        type = "Machine";
        position = new Vec2(( ( (int) pos.x / 40 ) * 40 ) + 20,
                            ( ( (int) pos.y / 40 ) * 40 ) + 20);
    }

    /**
     * Gets position of machine.
     *
     * @return Vec2 position of machine
     */
    public Vec2 getPos()
    {
        return position;
    }

    /**
     * Sets the position of machine. Will round to the nearest tile on screen.
     *
     * @param pos Vec2 new position.
     */
    public void setPos( Vec2 pos )
    {
        //Starting with (int)52/40 will return 1
        //Multiplied by 40 will return 40. Adding 20 then puts 53 at 60.
        //This ensures that a machines position will be at the center of a tile on a 40x40 tile layout.
        position = new Vec2(( ( (int) pos.x / 40 ) * 40 ) + 20,
                            ( ( (int) pos.y / 40 ) * 40 ) + 20);
    }

    /**
     * Implements FactoryEngineDrawable. Will draw the machine with orientation
     * on a FactoryEngine class.
     *
     * @param g Graphics2D graphic object on which the machine will be drawn.
     */
    @Override
    public void drawOnGraphic( Graphics2D g )
    {
        AffineTransform at = new AffineTransform();
        //First rotates the image to the rotation of one of the composites.
        at.rotate(( (Poly) composites.get(0).getShape() ).mat.getRadians(),
                  position.x, position.y);
        //Then moves image to place on screen. It is important to do this in this order,
        //as the rotation needs to be done at (0,0).
        at.translate(position.x - 20, position.y - 20);
        g.drawImage(img, at, null);
    }

    /**
     * Sets the orientation for the machine.
     *
     * @param radians Double value between -PI and PI.
     */
    public void setOrient( double radians )
    {
        for( Body b : composites ){
            b.setOrient(radians);
        }
    }

    /**
     * Sets the orientation for the machine.
     *
     * @param mat Matrix class.
     */
    public void setOrient( Matrix mat )
    {
        setOrient(mat.getRadians());
    }

    /**
     * Returns the name of a class. Machine by default. Name can be changed to
     * the name of a class that extends Machine.
     *
     * @return String name of class.
     */
    public String type()
    {
        return type;
    }

    /**
     * Will resolve any collision for a body against this machine.
     *
     * @param bodyA The body trying to collide with the machine.
     */
    public void resolveCollision( Body bodyA )
    {
        ArrayList<Manifold> manifolds = new ArrayList<>();
        for( Body bodyB : composites ){
            Manifold m = new Manifold(bodyA, bodyB);
            m.solve();

            if( m.contactCount > 0 ){
                manifolds.add(m);
            }
        }

        for( Manifold m : manifolds ){
            m.initialize();
            m.applyImpulse();
            m.positionalCorrection();
        }
    }

    /**
     * Abstract method ensures that any machine can be run by a thread.
     */
    @Override
    public abstract void run();
}
