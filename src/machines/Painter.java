package machines;

import baller.*;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import physXgine.*;

/**
 * A machine that will paint any items beneath it.
 *
 * @author warrm1
 */
public class Painter
        extends Machine
{
    public Color currentColor = MainFrame.blue;
    ;
    public PolyBox paintStrip;

    /**
     * Constructs a new Painter machine.
     *
     * @param pos Vec2 position on screen.
     */
    public Painter( Vec2 pos )
    {
        super("/Images/Painter.png", pos);
        type = "Painter";
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-20, 20), new Vec2(20, -20), new Vec2(20,
                                                                           20)));
    }

    /**
     * Overrides superclass's orient method so that this machine can not change
     * its orientation.
     * It will however change the currentColor.
     *
     * @param radians Double radian value.
     */
    @Override
    public void setOrient( double radians )
    {
        if( radians == 0 ){
            currentColor = MainFrame.green;
        }
        if( radians < -1 ){
            currentColor = MainFrame.red;
        }
        if( radians > 1 ){
            currentColor = MainFrame.blue;
        }
    }

    /**
     * Overrides superclass's orient method so that this machine can not change
     * its orientation.
     * It will however change the currentColor.
     *
     * @param mat Matrix class.
     */
    @Override
    public void setOrient( Matrix mat )
    {
        setOrient(mat.getRadians());
    }

    /**
     * Implements FactoryEngineDrawable. Overrides the superclass's draw method
     * to instead draw the machines image and its paintStrip.
     *
     * @param g Graphics2D graphic object on which the machine will be drawn.
     */
    @Override
    public void drawOnGraphic( Graphics2D g )
    {
        //Draws the paintStrip first so its underneath the machine.
        if( paintStrip != null ){
            paintStrip.drawOnGraphic(g);
        }
        //Draws the image.
        super.drawOnGraphic(g);
    }

    /**
     * Returns the runnable for this machine that should be used in a separate
     * thread to run the paintStrip.
     *
     * @param m    This painter machine.
     * @param comp The FactoryEngine to be drawn on.
     *
     * @return Runnable for running the paintStrip.
     */
    public Runnable getRun( Painter m, FactoryEngine comp )
    {
        Runnable r = () -> {
            //Sets the paintStrip position and size.
            paintStrip = new PolyBox(position.x, position.y, 0, new Vec2(-5, 0),
                                     new Vec2(5, 0), new Vec2(-5, 20), new Vec2(
                                             5, 20));
            paintStrip.getShape().set(new Vec2(-5, 0), new Vec2(5, 0), new Vec2(
                                      -5,
                                      20 + ( comp.getBounds().getHeight() - position.y )),
                                      new Vec2(5,
                                               20 + ( comp.getBounds().getHeight() - position.y )));
            boolean hitSomething;
            try{
                while( !Thread.currentThread().isInterrupted() ){
                    //The currentColor can be changed during this while loop.
                    paintStrip.colour = currentColor;
                    hitSomething = false;
                    //Runs through the machines under this machines column.
                    for( int i = ( (int) position.y / 40 ) + 1; i < comp.mitems[(int) ( position.x / 40 )].length; i++ ){
                        if( comp.mitems[(int) ( position.x / 40 )][i] != null ){
                            //If there is another painter underneath, then the paintStrip will set its size accordingly.
                            if( comp.mitems[(int) ( position.x / 40 )][i].type().equals(
                                    "Painter") ){
                                hitSomething = true;
                                paintStrip.getShape().set(new Vec2(-5, 0),
                                                          new Vec2(5, 0),
                                                          new Vec2(-5,
                                                                   20 + ( ( i * 40 ) - position.y )),
                                                          new Vec2(5,
                                                                   20 + ( ( i * 40 ) - position.y )));
                                break;
                            }
                        }
                    }
                    //If it does not hit anything at all the paintStrip will set its size to the bounds of teh screen.
                    if( !hitSomething ){
                        paintStrip.getShape().set(new Vec2(-5, 0),
                                                  new Vec2(5, 0), new Vec2(-5,
                                                                           20 + ( comp.getBounds().getHeight() - position.y )),
                                                  new Vec2(5,
                                                           20 + ( comp.getBounds().getHeight() - position.y )));
                    }
                    //Will only run if there are items on the screen.
                    if( comp.getItems().size() > 0 ){
                        try{
                            ArrayList<Manifold> manifolds = Collision.getCollision(
                                    paintStrip, comp.getItems());
                            for( Manifold manifold : manifolds ){
                                manifold.a.colour = m.currentColor;
                                manifold.b.colour = m.currentColor;
                            }
                        }catch( NullPointerException e ){
                        }
                    }
                    Thread.sleep(10);
                }
            }catch( InterruptedException e ){
            }
            paintStrip = null;
        };
        return r;
    }

    /**
     * Can be run as is required by Machine, but whence run it will do nothing.
     */
    @Override
    public void run()
    {
    }
}
