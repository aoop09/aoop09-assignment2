package machines;

import baller.PolyBox;
import physXgine.*;

/**
 * A machine that serves only as a hit box or tube walls. It has two walls
 * either side.
 *
 * @author warrm1
 */
public class TubeWall
        extends Machine
{
    /**
     * Constructs a TubeWall machine at the given position.
     *
     * @param pos Vec2 position on FactoryEngine to be used and drawn.
     */
    public TubeWall( Vec2 pos )
    {
        super("/Images/TubeWall.png", pos);
        type = "TubeWall";
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-12, -20), new Vec2(-20, 20), new Vec2(-12,
                                                                            20)));
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(12, -20),
                                   new Vec2(20, -20), new Vec2(12, 20),
                                   new Vec2(20, 20)));
    }

    /**
     * Can be run as is required by Machine, but whence run it will do nothing.
     */
    @Override
    public void run()
    {
    }
}
