package machines;

import baller.*;
import java.awt.Graphics2D;
import java.util.ArrayList;
import physXgine.*;

/**
 *
 * @author warrm1
 */
public class Squisher
        extends Machine
{
    Body bodyHeld;

    public Squisher( Vec2 pos, FactoryEngine comp )
    {
        super("/Images/TubeFloor.png", pos);
        type = "Squisher";
        environment = comp;

        //PolyBox's used to create a concave polygon hitbox.
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-12, -20), new Vec2(-20, 20), new Vec2(-12,
                                                                            20)));
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(12, -20),
                                   new Vec2(20, -20), new Vec2(12, 20),
                                   new Vec2(20, 20)));
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-20, -12), new Vec2(20, -20), new Vec2(20,
                                                                            -12)));
    }

    public void setOrient( double radians )
    {
    }

    @Override
    public void drawOnGraphic( Graphics2D g )
    {
        for( Body b : composites ){
            ( (FactoryItem) b ).drawOnGraphic(g);
        }
    }

    @Override
    public void resolveCollision( Body bodyA )
    {
        if( bodyA == bodyHeld ){
            Vec2 posAM = new Vec2(bodyA.position.sub(position));
            if( !bodyHeld.getShape().type.equals("Polygon") ){
                //Position against Machine
                if( posAM.y > -5 || posAM.y < 5 ){
                    //Spawns square at center of tile.
                    Square square = new Square(position.x - 7, position.y - 7,
                                               14, 14);
                    square.colour = bodyA.colour;
                    //Square needs to know what Panel its on.
                    square.enviroment = environment;
                    environment.removeItem(bodyA);
                    environment.addItem(square);
                    bodyHeld = square;
                }
            }
            if( posAM.y > 20 ){
                System.out.println(true);
                bodyHeld = null;
            }
        }else{
            Body bodyB;
            ArrayList<Manifold> manifolds = new ArrayList<>();
            for( int i = 0; i < 3; i++ ){
                if( i == 2 && bodyHeld == null ){
                    bodyHeld = bodyA;
                }else{
                    bodyB = composites.get(i);
                    Manifold m = new Manifold(bodyA, bodyB);
                    m.solve();

                    if( m.contactCount > 0 ){
                        manifolds.add(m);
                    }
                }
            }
            for( Manifold m : manifolds ){
                m.initialize();
                m.applyImpulse();
                m.positionalCorrection();
            }
        }
    }

    @Override
    public void run()
    {
    }
}
