package machines;

import baller.*;
import java.awt.Graphics2D;
import java.util.ArrayList;
import physXgine.*;

/**
 * A machine that exists as a floor hit-box, and will apply a force either left
 * or right to any Body that touches it.
 * Can only be rotated left and right.
 *
 * @author warrm1
 */
public class Conveyor
        extends Machine
{
    //This static value keeps all Conveyors in sync.
    public static int animateWidth;
    public Vec2 appliedForce = new Vec2();

    //offset and animateMul are used to flip the animation from right to left.
    public int offset = 0;
    public int animateMul = 1;

    /**
     * Constructs a new Conveyor machine.
     *
     * @param pos Vec2 Position.
     */
    public Conveyor( Vec2 pos )
    {
        super("/Images/Conveyor.png", pos);
        type = "Conveyor";
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(-20, 12),
                                   new Vec2(-20, 20), new Vec2(20, 12),
                                   new Vec2(20, 20)));
    }

    /**
     * Overrides superclass's orient method so that this machine can not change
     * its orientation.
     * It will however change the direction of its applied force.
     *
     * @param radians Double radian value.
     */
    @Override
    public void setOrient( double radians )
    {
        //Changing the offset value and animateMul changes the animation from right to left.
        if( radians > 0 ){
            appliedForce.set(100, 5);
            offset = 40;
            animateMul = -1;
        }else{
            appliedForce.set(-100, 5);
            offset = 0;
            animateMul = 1;
        }
    }

    /**
     * Overrides superclass's orient method so that this machine can not change
     * its orientation.
     * It will however change the direction of its applied force.
     *
     * @param mat Matrix class.
     */
    @Override
    public void setOrient( Matrix mat )
    {
        setOrient(mat.getRadians());
    }

    /**
     * Implements FactoryEngineDrawable. Overrides the superclass's draw method
     * to instead draw the machines image with an animation.
     *
     * @param g Graphics2D graphic object on which the machine will be drawn.
     */
    @Override
    public void drawOnGraphic( Graphics2D g )
    {
        //Original image is 80x40. This specifies that we draw in a 40x40 segmant
        //Then it goes through across its width, increasing the x value of what part of the image we draw.
        //So we could do, (0,40 to 40,40) then go (1,40 to 41,40) of the original 80x40
        g.drawImage(img, (int) position.x - 20, (int) position.y - 20,
                    (int) position.x + 20, (int) position.y + 20,
                    (int) offset + ( animateWidth * animateMul ), 0,
                    (int) offset + ( animateWidth * animateMul ) + 40, 40, null);
    }

    /**
     * Will resolve any collision for a body against this machine.
     * As well as resolving the collision it will also apply a force to the
     * Body.
     *
     * @param bodyA The body trying to collide with the machine.
     */
    @Override
    public void resolveCollision( Body bodyA )
    {
        ArrayList<Manifold> manifolds = new ArrayList<>();
        for( Body bodyB : composites ){
            Manifold m = new Manifold(bodyA, bodyB);
            m.solve();

            //If contact is made
            if( m.contactCount > 0 ){
                manifolds.add(m);
                bodyA.applyForce(appliedForce.mul(10));
                //Moves ball
                bodyA.move(( MainFrame.globalDT - bodyA.objDT ) * 5);
                //Resets forces
                bodyA.force.set(0, 0);
                bodyA.torque = 0;
            }
        }

        for( Manifold m : manifolds ){
            m.initialize();
            m.applyImpulse();
            m.positionalCorrection();
        }
    }

    /**
     * Returns the runnable that needs to be run in a thread. This runnable
     * helps animate all Conveyors on screen at once.
     *
     * @return Runnable for animating Conveyors.
     */
    public static Runnable getRun()
    {
        Runnable r = () -> {
            animateWidth = 0;
            try{
                while( !Thread.currentThread().isInterrupted() ){
                    animateWidth++;
                    if( animateWidth > 40 ){
                        animateWidth = 0;
                    }
                    Thread.sleep(20);
                }
            }catch( InterruptedException e ){
            }
        };
        return r;
    }

    /**
     * Can be run as is required by Machine, but whence run it will do nothing.
     */
    @Override
    public void run()
    {
    }
}
