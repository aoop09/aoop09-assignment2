package machines;

import baller.*;
import physXgine.*;

/**
 * Extends Machine. Is a machine that spawns a ball when run only if there is
 * nothing in the way.
 *
 * @author Mitchell
 */
public class BallDropper
        extends Machine
{
    /**
     * Constructs a BallDropper machine at the given position.
     *
     * @param pos  Vec2 position on FactoryEngine to be used and drawn.
     * @param comp FactoryEngine environment that the machine lives in.
     */
    public BallDropper( Vec2 pos, FactoryEngine comp )
    {
        super("/Images/BallDropper.png", pos);
        type = "BallDropper";
        environment = comp;

        //PolyBox's used to create a concave polygon hitbox.
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-20, -12), new Vec2(20, -20), new Vec2(20,
                                                                            -12)));
        composites.add(
                new PolyBox(position.x, position.y, 0, new Vec2(-20, -20),
                            new Vec2(-12, -20), new Vec2(-20, 20), new Vec2(-12,
                                                                            20)));
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(12, -20),
                                   new Vec2(20, -20), new Vec2(12, 20),
                                   new Vec2(20, 20)));
    }

    /**
     * Overrides superclass's orient method so that this machine can not change
     * its orientation.
     *
     * @param radians Double radian value.
     */
    @Override
    public void setOrient( double radians )
    {
    }

    /**
     * Overrides superclass's orient method so that this machine can not change
     * its orientation.
     *
     * @param mat Matrix class.
     */
    @Override
    public void setOrient( Matrix mat )
    {
    }

    /**
     * This run method can be used to spawn a ball at this machines position. If
     * spawning a ball will result in a collision, then no ball is spawned.
     */
    @Override
    public void run()
    {
        //Spawns ball at center of tile.
        Ball ball = new Ball(position.x, position.y);
        if( Collision.getCollision(ball, environment.getItems()).size() < 1 ){
            //Default blue color.
            ball.colour = MainFrame.blue;
            //Ball needs to know what Panel its on.
            ball.enviroment = environment;
            environment.addItem(ball);
        }
    }
}
