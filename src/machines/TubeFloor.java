package machines;

import baller.PolyBox;
import physXgine.*;

/**
 * A machine that serves only as a hit box or floor. is able to be rotated to
 * serve as a wall or roof.
 *
 * @author warrm1
 */
public class TubeFloor
        extends Machine
{
    /**
     * Constructs a TubeFloor machine at the given position.
     *
     * @param pos Vec2 position on FactoryEngine to be used and drawn.
     */
    public TubeFloor( Vec2 pos )
    {
        super("/Images/TubeFloor.png", pos);
        type = "TubeFloor";
        composites.add(new PolyBox(position.x, position.y, 0, new Vec2(-20, 12),
                                   new Vec2(-20, 20), new Vec2(20, 12),
                                   new Vec2(20, 20)));
    }

    /**
     * Can be run as is required by Machine, but whence run it will do nothing.
     */
    @Override
    public void run()
    {
    }
}
