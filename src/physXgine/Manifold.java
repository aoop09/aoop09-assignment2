package physXgine;

/**
 *
 * @author Mitchell
 */
public class Manifold
{

    public Body a;
    public Body b;
    public double penetration;
    public final Vec2 normal = new Vec2();
    public final Vec2[] contacts = { new Vec2(), new Vec2() };
    public int contactCount;
    public double e;
    public double df;
    public double sf;

    public Manifold( Body a, Body b )
    {
        this.a = a;
        this.b = b;
    }

    public void solve()
    {
        if( a.getShape().getType().equals("Circle") ){
            if( b.getShape().getType().equals("Circle") ){
                Collision.resolve(this, (Circle) a.getShape(),
                                  (Circle) b.getShape());
            }else if( b.getShape().getType().equals("Polygon") ){
                Collision.resolve(this, (Circle) a.getShape(),
                                  (Poly) b.getShape());
            }
        }else if( a.getShape().getType().equals("Polygon") ){
            if( b.getShape().getType().equals("Circle") ){
                Collision.resolve(this, (Poly) a.getShape(),
                                  (Circle) b.getShape());
            }else if( b.getShape().getType().equals("Polygon") ){
                Collision.resolve(this, (Poly) a.getShape(), (Poly) b.getShape());
            }
        }
    }

    public void initialize()
    {
        // Calculate average restitution
        // e = std::min( a->restitution, b->restitution );
        e = StrictMath.min(a.restitution, b.restitution);

        // Calculate static and dynamic friction
        // sf = std::sqrt( a->staticFriction * a->staticFriction );
        // df = std::sqrt( a->dynamicFriction * a->dynamicFriction );
        sf = (double) StrictMath.sqrt(
                a.staticFriction * a.staticFriction + b.staticFriction * b.staticFriction);
        df = (double) StrictMath.sqrt(
                a.dynamicFriction * a.dynamicFriction + b.dynamicFriction * b.dynamicFriction);

        for( int i = 0; i < contactCount; ++i ){
            // Calculate radii from COM to contact
            // Vec2 ra = contacts[i] - a->position;
            // Vec2 rb = contacts[i] - b->position;
            Vec2 ra = contacts[i].sub(a.position);
            Vec2 rb = contacts[i].sub(b.position);

            // Vec2 rv = b->velocity + Cross( b->angularVelocity, rb ) -
            // a->velocity - Cross( a->angularVelocity, ra );
            Vec2 rv = b.velocity.add(Vec2.cross(b.angularVelocity, rb,
                                                new Vec2())).subi(a.velocity).subi(
                    Vec2.cross(a.angularVelocity, ra, new Vec2()));

            // Determine if we should perform a resting collision or not
            // The idea is if the only thing moving this object is gravity,
            // then the collision should be performed without any restitution
            // if(rv.LenSqr( ) < (dt * gravity).LenSqr( ) + EPSILON)
            if( rv.lengthSq() < ImpulseMath.RESTING ){
                e = 0.0f;
            }
        }
    }

    public void applyImpulse()
    {
        // Early out and positional correct if both objects have infinite mass
        // if(Equal( a->im + b->im, 0 ))
        if( ImpulseMath.equal(a.inv_mass + b.inv_mass, 0) ){
            infiniteMassCorrection();
            return;
        }

        for( int i = 0; i < contactCount; ++i ){
            // Calculate radii from COM to contact
            // Vec2 ra = contacts[i] - a->position;
            // Vec2 rb = contacts[i] - b->position;
            Vec2 ra = contacts[i].sub(a.position);
            Vec2 rb = contacts[i].sub(b.position);

            // Relative velocity
            // Vec2 rv = b->velocity + Cross( b->angularVelocity, rb ) -
            // a->velocity - Cross( a->angularVelocity, ra );
            Vec2 rv = b.velocity.add(Vec2.cross(b.angularVelocity, rb,
                                                new Vec2())).subi(a.velocity).subi(
                    Vec2.cross(a.angularVelocity, ra, new Vec2()));

            // Relative velocity along the normal
            // real contactVel = Dot( rv, normal );
            double contactVel = Vec2.dot(rv, normal);

            // Do not resolve if velocities are separating
            if( contactVel > 0 ){
                return;
            }

            // real raCrossN = Cross( ra, normal );
            // real rbCrossN = Cross( rb, normal );
            // real inv_massSum = a->im + b->im + Sqr( raCrossN ) * a->iI + Sqr(
            // rbCrossN ) * b->iI;
            double raCrossN = Vec2.cross(ra, normal);
            double rbCrossN = Vec2.cross(rb, normal);
            double inv_massSum = a.inv_mass + b.inv_mass + ( raCrossN * raCrossN ) * a.inv_inertia + ( rbCrossN * rbCrossN ) * b.inv_inertia;

            // Calculate impulse scalar
            double j = -( 1.0f + e ) * contactVel;
            j /= inv_massSum;
            j /= contactCount;

            // Apply impulse
            Vec2 impulse = normal.mul(j);
            a.applyImpulse(impulse.neg(), ra);
            b.applyImpulse(impulse, rb);

            // Friction impulse
            // rv = b->velocity + Cross( b->angularVelocity, rb ) -
            // a->velocity - Cross( a->angularVelocity, ra );
            rv = b.velocity.add(Vec2.cross(b.angularVelocity, rb, new Vec2())).subi(
                    a.velocity).subi(Vec2.cross(a.angularVelocity, ra,
                                                new Vec2()));

            // Vec2 t = rv - (normal * Dot( rv, normal ));
            // t.Normalize( );
            Vec2 t = new Vec2(rv);
            t.addsi(normal, -Vec2.dot(rv, normal));
            t.normalize();

            // j tangent magnitude
            double jt = -Vec2.dot(rv, t);
            jt /= inv_massSum;
            jt /= contactCount;

            // Don't apply tiny friction impulses
            if( ImpulseMath.equal(jt, 0.0f) ){
                return;
            }

            // Coulumb's law
            Vec2 tangentImpulse;
            // if(std::abs( jt ) < j * sf)
            if( StrictMath.abs(jt) < j * sf ){
                // tangentImpulse = t * jt;
                tangentImpulse = t.mul(jt);
            }else{
                // tangentImpulse = t * -j * df;
                tangentImpulse = t.mul(j).muli(-df);
            }

            // Apply friction impulse
            // a->ApplyImpulse( -tangentImpulse, ra );
            // b->ApplyImpulse( tangentImpulse, rb );
            a.applyImpulse(tangentImpulse.neg(), ra);
            b.applyImpulse(tangentImpulse, rb);
        }
    }

    public void positionalCorrection()
    {
        // const real k_slop = 0.05f; // Penetration allowance
        // const real percent = 0.4f; // Penetration percentage to correct
        // Vec2 correction = (std::max( penetration - k_slop, 0.0f ) / (a->im +
        // b->im)) * normal * percent;
        // a->position -= correction * a->im;
        // b->position += correction * b->im;

        double correction = StrictMath.max(
                penetration - ImpulseMath.PENETRATION_ALLOWANCE, 0.0f) / ( a.inv_mass + b.inv_mass ) * ImpulseMath.PENETRATION_CORRETION;

        a.position.addsi(normal, -a.inv_mass * correction);
        b.position.addsi(normal, b.inv_mass * correction);
    }

    public void infiniteMassCorrection()
    {
        a.velocity.set(0, 0);
        b.velocity.set(0, 0);
    }

}
