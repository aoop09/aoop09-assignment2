package physXgine;

import java.util.*;

/**
 * Provides static methods for resolving collisions.
 *
 * @author Mitchell
 */
public class Collision
{

    /**
     * Will resolve all collisions from a list of Bodys.
     *
     * @param items List of Bodies.
     */
    public static void resolveCollisions( List<Body> items )
    {
        try{
            //A manifold holds onto the collision information.
            //Manifolds use Collision to solve the collisions.
            ArrayList<Manifold> manifolds = new ArrayList<>();
            for( int i = 0; i < items.size(); ++i ){
                Body A = items.get(i);

                for( int j = i + 1; j < items.size(); ++j ){
                    Body B = items.get(j);

                    //If two static Bodys are overlapping.
                    if( A.mass == 0 && B.mass == 0 ){
                        continue;
                    }

                    Manifold m = new Manifold(A, B);
                    m.solve();

                    //If contact is made, then add the manifold to teh list.
                    if( m.contactCount > 0 ){
                        manifolds.add(m);
                    }
                }
            }

            for( Manifold m : manifolds ){
                m.initialize();
                m.applyImpulse();
                m.positionalCorrection();
            }
        }catch( Exception e ){
        }
    }

    /**
     * Will resolve all collisions of a Body against a list of Bodys.
     *
     * @param A     A given Body to compare against the list.
     * @param items List of Bodies.
     */
    public static void resolveCollision( Body A, List<Body> items )
    {
        ArrayList<Manifold> manifolds = new ArrayList<>();
        for( int j = 0; j < items.size(); ++j ){
            try{
                Body B = items.get(j);
                if( A.position.distance(B.position) > 40 ){
                    continue;
                }
                if( A == B ){
                    continue;
                }
                if( A.mass == 0 && B.mass == 0 ){
                    continue;
                }

                Manifold m = new Manifold(A, B);
                m.solve();

                if( m.contactCount > 0 ){
                    manifolds.add(m);
                }
            }catch( ConcurrentModificationException | IndexOutOfBoundsException e ){
            }
        }

        for( Manifold m : manifolds ){
            m.initialize();
            m.applyImpulse();
            m.positionalCorrection();
        }
    }

    /**
     * Will return a list of Manifolds describing all collisions against a given
     * Body.
     *
     * @param A     A given Body to compare against the list.
     * @param items List of Bodies.
     *
     * @return ArrayList of Manifolds. The list of collisions against the given
     *         Body.
     */
    public static ArrayList<Manifold> getCollision( Body A, List<Body> items )
    {
        ArrayList<Manifold> manifolds = new ArrayList<>();
        for( int j = 0; j < items.size(); ++j ){
            try{
                Body B = items.get(j);
                if( A == B ){
                    continue;
                }
                if( A.mass == 0 && B.mass == 0 ){
                    continue;
                }

                Manifold m = new Manifold(A, B);
                m.solve();

                if( m.contactCount > 0 ){
                    manifolds.add(m);
                }
            }catch( ConcurrentModificationException | IndexOutOfBoundsException e ){
            }
        }
        return manifolds;
    }

    /**
     * Resolves the collision of two Circles using a given Manifold.
     *
     * @param m Manifold to hold information about the collision.
     * @param a First Circle.
     * @param b Second Circle.
     */
    public static void resolve( Manifold m, Circle a, Circle b )
    {
        double radius = a.radius + a.radius;

        // Not in contact
        if( b.body.position.sub(a.body.position).lengthSq() >= radius * radius ){
            m.contactCount = 0;
            return;
        }
        // Calculate translational vector, which is normal
        // Vec2 normal = b->position - a->position;
        Vec2 normal = b.body.position.sub(a.body.position);

        // real dist_sqr = normal.LenSqr( );
        // real radius = a->radius + b->radius;
        double dist_sqr = normal.lengthSq();

        double distance = (double) StrictMath.sqrt(dist_sqr);

        m.contactCount = 1;

        if( distance == 0.0f ){
            // m->penetration = a->radius;
            // m->normal = Vec2( 1, 0 );
            // m->contacts [0] = a->position;
            m.penetration = a.radius;
            m.normal.set(1.0f, 0.0f);
            m.contacts[0].set(a.body.position);
        }else{
            // m->penetration = radius - distance;
            // m->normal = normal / distance; // Faster than using Normalized since
            // we already performed sqrt
            // m->contacts[0] = m->normal * a->radius + a->position;
            m.penetration = radius - distance;
            m.normal.set(normal).divi(distance);
            m.contacts[0].set(m.normal).muli(a.radius).addi(a.body.position);
        }
    }

    /**
     * Resolves the collision of a Circle and a Poly using a given Manifold.
     *
     * @param m Manifold to hold information about the collision.
     * @param a A Circle.
     * @param b A Poly.
     */
    public static void resolve( Manifold m, Circle a, Poly b )
    {
        m.contactCount = 0;

        // Transform circle center to Polygon model space
        // Vec2 center = a->position;
        // center = b->u.Transpose( ) * (center - b->position);
        Vec2 center = b.mat.transpose().muli(
                a.body.position.sub(b.body.position));

        // Find edge with minimum penetration
        // Exact concept as using support points in Polygon vs Polygon
        double separation = -Double.MAX_VALUE;
        int faceNormal = 0;
        for( int i = 0; i < b.vertexCount; ++i ){
            // real s = Dot( b->m_normals[i], center - b->m_vertices[i] );
            double s = Vec2.dot(b.normals[i], center.sub(b.vertices[i]));

            if( s > a.radius ){
                return;
            }

            if( s > separation ){
                separation = s;
                faceNormal = i;
            }
        }

        // Grab face's vertices
        Vec2 v1 = b.vertices[faceNormal];
        int i2 = faceNormal + 1 < b.vertexCount ? faceNormal + 1 : 0;
        Vec2 v2 = b.vertices[i2];

        // Check to see if center is within polygon
        if( separation < ImpulseMath.EPSILON ){
            // m->contact_count = 1;
            // m->normal = -(b->u * b->m_normals[faceNormal]);
            // m->contacts[0] = m->normal * a->radius + a->position;
            // m->penetration = a->radius;

            m.contactCount = 1;
            b.mat.mul(b.normals[faceNormal], m.normal).negi();
            m.contacts[0].set(m.normal).muli(a.radius).addi(a.body.position);
            m.penetration = a.radius;
            return;
        }

        // Determine which voronoi region of the edge center of circle lies within
        // real dot1 = Dot( center - v1, v2 - v1 );
        // real dot2 = Dot( center - v2, v1 - v2 );
        // m->penetration = a->radius - separation;
        double dot1 = Vec2.dot(center.sub(v1), v2.sub(v1));
        double dot2 = Vec2.dot(center.sub(v2), v1.sub(v2));
        m.penetration = a.radius - separation;

        // Closest to v1
        if( dot1 <= 0.0f ){
            if( Vec2.distanceSq(center, v1) > a.radius * a.radius ){
                return;
            }

            // m->contact_count = 1;
            // Vec2 n = v1 - center;
            // n = b->u * n;
            // n.Normalize( );
            // m->normal = n;
            // v1 = b->u * v1 + b->position;
            // m->contacts[0] = v1;
            m.contactCount = 1;
            b.mat.muli(m.normal.set(v1).subi(center)).normalize();
            b.mat.mul(v1, m.contacts[0]).addi(b.body.position);
        } // Closest to v2
        else if( dot2 <= 0.0f ){
            if( Vec2.distanceSq(center, v2) > a.radius * a.radius ){
                return;
            }

            // m->contact_count = 1;
            // Vec2 n = v2 - center;
            // v2 = b->u * v2 + b->position;
            // m->contacts[0] = v2;
            // n = b->u * n;
            // n.Normalize( );
            // m->normal = n;
            m.contactCount = 1;
            b.mat.muli(m.normal.set(v2).subi(center)).normalize();
            b.mat.mul(v2, m.contacts[0]).addi(b.body.position);
        } // Closest to face
        else{
            Vec2 n = b.normals[faceNormal];

            if( Vec2.dot(center.sub(v1), n) > a.radius ){
                return;
            }

            // n = b->u * n;
            // m->normal = -n;
            // m->contacts[0] = m->normal * a->radius + a->position;
            // m->contact_count = 1;
            m.contactCount = 1;
            b.mat.mul(n, m.normal).negi();
            m.contacts[0].set(a.body.position).addsi(m.normal, a.radius);
        }
    }

    /**
     * Resolves the collision of a Poly against a Circle using a given Manifold.
     *
     * @param m Manifold to hold information about the collision.
     * @param a A Poly.
     * @param b A Circle
     */
    public static void resolve( Manifold m, Poly a, Circle b )
    {
        resolve(m, b, a);
        if( m.contactCount > 0 ){
            m.normal.negi();
        }
    }

    private static double findAxisLeastPenetration( int[] faceIndex, Poly a, Poly b )
    {
        double bestDistance = -Float.MAX_VALUE;
        int bestIndex = 0;

        for( int i = 0; i < a.vertexCount; ++i ){
            // Retrieve a face normal from a
            // Vec2 n = a->m_normals[i];
            // Vec2 nw = a->u * n;
            Vec2 nw = a.mat.mul(a.normals[i]);

            // Transform face normal into b's model space
            // Matrix buT = b->u.Transpose( );
            // n = buT * nw;
            Matrix buT = b.mat.transpose();
            Vec2 n = buT.mul(nw);

            // Retrieve support point from b along -n
            // Vec2 s = b->GetSupport( -n );
            Vec2 s = b.getSupport(n.neg());

            // Retrieve vertex on face from a, transform into
            // b's model space
            // Vec2 v = a->m_vertices[i];
            // v = a->u * v + a->body->position;
            // v -= b->body->position;
            // v = buT * v;
            Vec2 v = buT.muli(
                    a.mat.mul(a.vertices[i]).addi(a.body.position).subi(
                    b.body.position));

            // Compute penetration distance (in b's model space)
            // real d = Dot( n, s - v );
            double d = Vec2.dot(n, s.sub(v));

            // Store greatest distance
            if( d > bestDistance ){
                bestDistance = d;
                bestIndex = i;
            }
        }

        faceIndex[0] = bestIndex;
        return bestDistance;
    }

    private static void findIncidentFace( Vec2[] v, Poly RefPoly, Poly IncPoly, int referenceIndex )
    {
        Vec2 referenceNormal = RefPoly.normals[referenceIndex];

        // Calculate normal in incident's frame of reference
        // referenceNormal = RefPoly->u * referenceNormal; // To world space
        // referenceNormal = IncPoly->u.Transpose( ) * referenceNormal; // To
        // incident's model space
        referenceNormal = RefPoly.mat.mul(referenceNormal); // To world space
        referenceNormal = IncPoly.mat.transpose().mul(referenceNormal); // To
        // incident's
        // model
        // space

        // Find most anti-normal face on incident polygon
        int incidentFace = 0;
        double minDot = Float.MAX_VALUE;
        for( int i = 0; i < IncPoly.vertexCount; ++i ){
            // real dot = Dot( referenceNormal, IncPoly->m_normals[i] );
            double dot = Vec2.dot(referenceNormal, IncPoly.normals[i]);

            if( dot < minDot ){
                minDot = dot;
                incidentFace = i;
            }
        }

        // assign face vertices for incidentFace
        // v[0] = IncPoly->u * IncPoly->m_vertices[incidentFace] +
        // IncPoly->body->position;
        // incidentFace = incidentFace + 1 >= (int32)IncPoly->m_vertexCount ? 0 :
        // incidentFace + 1;
        // v[1] = IncPoly->u * IncPoly->m_vertices[incidentFace] +
        // IncPoly->body->position;
        v[0] = IncPoly.mat.mul(IncPoly.vertices[incidentFace]).addi(
                IncPoly.body.position);
        incidentFace = incidentFace + 1 >= (int) IncPoly.vertexCount ? 0 : incidentFace + 1;
        v[1] = IncPoly.mat.mul(IncPoly.vertices[incidentFace]).addi(
                IncPoly.body.position);
    }

    private static int clip( Vec2 n, double c, Vec2[] face )
    {
        int sp = 0;
        Vec2[] out = {
            new Vec2(face[0]),
            new Vec2(face[1])
        };

        // Retrieve distances from each endpoint to the line
        // d = ax + by - c
        // real d1 = Dot( n, face[0] ) - c;
        // real d2 = Dot( n, face[1] ) - c;
        double d1 = Vec2.dot(n, face[0]) - c;
        double d2 = Vec2.dot(n, face[1]) - c;

        // If negative (behind plane) clip
        // if(d1 <= 0.0f) out[sp++] = face[0];
        // if(d2 <= 0.0f) out[sp++] = face[1];
        if( d1 <= 0.0f ){
            out[sp++].set(face[0]);
        }
        if( d2 <= 0.0f ){
            out[sp++].set(face[1]);
        }

        // If the points are on different sides of the plane
        if( d1 * d2 < 0.0f ) // less than to ignore -0.0f
        {
            // Push intersection point
            // real alpha = d1 / (d1 - d2);
            // out[sp] = face[0] + alpha * (face[1] - face[0]);
            // ++sp;

            double alpha = d1 / ( d1 - d2 );

            out[sp++].set(face[1]).subi(face[0]).muli(alpha).addi(face[0]);
        }

        // assign our new converted values
        face[0] = out[0];
        face[1] = out[1];

        // assert( sp != 3 );
        return sp;
    }

    /**
     * Resolves the collision of two Polys using a given Manifold.
     *
     * @param m Manifold to hold information about the collision.
     * @param a First Poly.
     * @param b Second Poly.
     */
    public static void resolve( Manifold m, Poly a, Poly b )
    {
        m.contactCount = 0;

        // Check for a separating axis with a's face planes
        int[] faceA = { 0 };
        double penetrationA = findAxisLeastPenetration(faceA, a, b);
        if( penetrationA >= 0.0f ){
            return;
        }

        // Check for a separating axis with b's face planes
        int[] faceB = { 0 };
        double penetrationB = findAxisLeastPenetration(faceB, b, a);
        if( penetrationB >= 0.0f ){
            return;
        }

        int referenceIndex;
        boolean flip; // always point from a to b

        Poly RefPoly; // Reference
        Poly IncPoly; // Incident

        // Determine which shape contains reference face
        if( ImpulseMath.gt(penetrationA, penetrationB) ){
            RefPoly = a;
            IncPoly = b;
            referenceIndex = faceA[0];
            flip = false;
        }else{
            RefPoly = b;
            IncPoly = a;
            referenceIndex = faceB[0];
            flip = true;
        }

        // World space incident face
        Vec2[] incidentFace = Vec2.arrayOf(2);

        findIncidentFace(incidentFace, RefPoly, IncPoly, referenceIndex);

        // y
        // ^ .n ^
        // +---c ------posPlane--
        // x < | i |\
        // +---+ c-----negPlane--
        // \ v
        // r
        //
        // r : reference face
        // i : incident poly
        // c : clipped point
        // n : incident normal
        // Setup reference face vertices
        Vec2 v1 = RefPoly.vertices[referenceIndex];
        referenceIndex = referenceIndex + 1 == RefPoly.vertexCount ? 0 : referenceIndex + 1;
        Vec2 v2 = RefPoly.vertices[referenceIndex];

        // Transform vertices to world space
        // v1 = RefPoly->u * v1 + RefPoly->body->position;
        // v2 = RefPoly->u * v2 + RefPoly->body->position;
        v1 = RefPoly.mat.mul(v1).addi(RefPoly.body.position);
        v2 = RefPoly.mat.mul(v2).addi(RefPoly.body.position);

        // Calculate reference face side normal in world space
        // Vec2 sidePlaneNormal = (v2 - v1);
        // sidePlaneNormal.Normalize( );
        Vec2 sidePlaneNormal = v2.sub(v1);
        sidePlaneNormal.normalize();

        // Orthogonalize
        // Vec2 refFaceNormal( sidePlaneNormal.y, -sidePlaneNormal.x );
        Vec2 refFaceNormal = new Vec2(sidePlaneNormal.y, -sidePlaneNormal.x);

        // ax + by = c
        // c is distance from origin
        // real refC = Dot( refFaceNormal, v1 );
        // real negSide = -Dot( sidePlaneNormal, v1 );
        // real posSide = Dot( sidePlaneNormal, v2 );
        double refC = Vec2.dot(refFaceNormal, v1);
        double negSide = -Vec2.dot(sidePlaneNormal, v1);
        double posSide = Vec2.dot(sidePlaneNormal, v2);

        // Clip incident face to reference face side planes
        // if(Clip( -sidePlaneNormal, negSide, incidentFace ) < 2)
        if( clip(sidePlaneNormal.neg(), negSide, incidentFace) < 2 ){
            return; // Due to doubleing point error, possible to not have required
            // points
        }

        // if(Clip( sidePlaneNormal, posSide, incidentFace ) < 2)
        if( clip(sidePlaneNormal, posSide, incidentFace) < 2 ){
            return; // Due to doubleing point error, possible to not have required
            // points
        }

        // Flip
        m.normal.set(refFaceNormal);
        if( flip ){
            m.normal.negi();
        }

        // Keep points behind reference face
        int cp = 0; // clipped points behind reference face
        double separation = Vec2.dot(refFaceNormal, incidentFace[0]) - refC;
        if( separation <= 0.0f ){
            m.contacts[cp].set(incidentFace[0]);
            m.penetration = -separation;
            ++cp;
        }else{
            m.penetration = 0;
        }

        separation = Vec2.dot(refFaceNormal, incidentFace[1]) - refC;

        if( separation <= 0.0f ){
            m.contacts[cp].set(incidentFace[1]);

            m.penetration += -separation;
            ++cp;

            // average penetration
            m.penetration /= cp;
        }

        m.contactCount = cp;
    }
}
