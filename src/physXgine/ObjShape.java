package physXgine;

/**
 * Abstract shape class.
 *
 * @author Mitchell
 */
public abstract class ObjShape
{
    public Body body;
    public String type;

    public abstract void computeMass( double density );

    public abstract void setOrient( double radians );

    public abstract void setOrient( Matrix mat );

    public abstract String getType();
}
