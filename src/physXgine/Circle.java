package physXgine;

/**
 * Extends ObjShape. Holds the shape of a Circle.
 *
 * @author Mitchell
 */
public class Circle
        extends ObjShape
{

    public double x;
    public double y;
    public double radius;

    /**
     * Constructs a Circle with a given radius and body.
     *
     * @param radius_ Radius of circle as a Double.
     * @param body_   The Body this shape belongs to.
     */
    public Circle( double radius_, Body body_ )
    {
        body = body_;
        radius = radius_;
    }

    /**
     * Computes the mass of the Body with a given density.
     *
     * @param density Double number describing the density. 0 will set mass to
     *                static.
     */
    @Override
    public void computeMass( double density )
    {
        body.mass = ImpulseMath.PI * radius * radius * density;
        body.inv_mass = ( body.mass != 0.0f ) ? 1.0f / body.mass : 0.0f;
        body.inertia = body.mass * radius * radius;
        body.inv_inertia = ( body.inertia != 0.0f ) ? 1.0f / body.inertia : 0.0f;
    }

    /**
     * Returns the name of this shape.
     *
     * @return String name of this shape: Circle.
     */
    @Override
    public String getType()
    {
        return "Circle";
    }

    /**
     * Overrides the abstract ObjShape method. Does nothing.
     *
     * @param radians Double, radian position of rotation.
     */
    @Override
    public void setOrient( double radians )
    {
    }

    /**
     * Overrides the abstract ObjShape method. Does nothing.
     *
     * @param mat Matrix describing rotation.
     */
    @Override
    public void setOrient( Matrix mat )
    {
        setOrient(mat.getRadians());
    }

}
