package physXgine;

import java.awt.*;

/**
 * A physics Object that takes an ObjShape as a hit-box.
 *
 * @author Mitchell
 */
public abstract class Body
{

    /**
     * A settable color that can be used when drawing this Body.
     */
    public Color colour = Color.BLUE;

    /**
     * Used as a modifier to set the shapes Mass. Set 0 for a static Body.
     */
    public double density;

    /**
     * Determines how bouncy the Body is 0-1
     */
    public double restitution;

    /**
     * Force required to start moving.
     */
    public double staticFriction;

    /**
     * Force lost with contact.
     */
    public double dynamicFriction;

    /**
     * How heavy this Body is. Set with Shape.computeMass();
     */
    public double mass;

    /**
     * 1/mass
     */
    public double inv_mass;

    /**
     * How much inertia this Body gets. Set with Shape.computeMass();
     */
    public double inertia;

    /**
     * 1/inertia
     */
    public double inv_inertia;

    /**
     * Vector describing this Bodys position.
     */
    public Vec2 position;

    /**
     * Vector describing this Bodys velocity in relation to its position.
     */
    public Vec2 velocity;

    /**
     * Vector describing the added forces impacted unto this Body per move
     * cycle.
     * Needs to be reset before moving again.
     */
    public Vec2 force;

    /**
     * Force of spin.
     */
    public double angularVelocity;

    /**
     * Power of spin.
     */
    public double torque;

    /**
     * Radian position of rotation.
     */
    public double orient;

    /**
     * Last time cycle in relation to Global time that this Body moved.
     */
    public double objDT;

    /**
     * Constructs a new Body. Resets all important variables to default.
     * Sets position to (0, 0).
     */
    public Body()
    {
        position = new Vec2(0, 0);
        velocity = new Vec2(0, 0);
        force = new Vec2(0, 0);
        torque = 0;
        orient = 0;
        staticFriction = 0.4d;
        dynamicFriction = 0.3d;
        restitution = 0.2d;
    }

    /**
     * Sets this Body's variables so that it is static.
     */
    public void setStatic()
    {
        mass = 0.0d;
        inv_mass = 0.0d;
        inertia = 0.0d;
        inv_inertia = 0.0d;
        dynamicFriction = 0.0d;
        staticFriction = 0.0d;
        restitution = 0.9d;
    }

    /**
     * Applies a force to this Body's velocity.
     *
     * @param impulse       Vector of force.
     * @param contactVector Vector of contact from which the force is applied.
     */
    public void applyImpulse( Vec2 impulse, Vec2 contactVector )
    {
        // velocity += im * impulse;
        // angularVelocity += iI * Cross( contactVector, impulse );

        velocity.addsi(impulse, inv_mass);
        angularVelocity += inv_inertia * Vec2.cross(contactVector, impulse);
    }

    /**
     * Adds a force to all the forces acting on this Body.
     *
     * @param f Vector of force.
     */
    public void applyForce( Vec2 f )
    {
        force.addi(f);
    }

    /**
     * This method is required so that the Body can rotate.
     *
     * @param radians Double. Rotation angle in radians
     */
    public abstract void setOrient( double radians );

    /**
     * This method is required so that the Body can rotate.
     *
     * @param mat Matrix containing rotational information.
     */
    public abstract void setOrient( Matrix mat );

    /**
     * Returns back the shape contained by this Body.
     *
     * @return shape extending ObjShape.
     */
    public abstract ObjShape getShape();

    /**
     * Integrates all forces acting on this Body to its velocity and angular
     * velocity.
     *
     * @param dt Double. Time moving.
     */
    public void integrateForces( double dt )
    {
        if( inv_mass == 0.0f ){
            return;
        }

        double dts = dt * 0.5f;

        velocity.addsi(force, inv_mass * dts);
        velocity.addsi(ImpulseMath.GRAVITY, dts);
        angularVelocity += torque * inv_inertia * dts;
    }

    /**
     * Integrates velocity into this Bodys position.
     *
     * @param dt Double. Time moved.
     */
    public void integrateVelocity( double dt )
    {
        if( inv_mass == 0.0f ){
            return;
        }

        position.addsi(velocity, dt);
        orient += angularVelocity * dt;
        setOrient(orient);

        integrateForces(dt);
    }

    /**
     * Moves this Body along its velocity and forces.
     * Takes a length of time as a scalar.
     *
     * @param dt Double. Scalar of time moving.
     */
    public void move( double dt )
    {
        integrateForces(dt);
        integrateVelocity(dt);
    }
}
