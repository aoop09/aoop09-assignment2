package physXgine;

/**
 * Extends ObjShape. Holds the shape of a Polygon.
 *
 * @author Mitchell
 */
public class Poly
        extends ObjShape
{

    public int vertexCount;
    public final int MAX_POLY_VERTEX_COUNT = 64;
    public Vec2[] vertices = new Vec2[MAX_POLY_VERTEX_COUNT];
    public Vec2[] normals = new Vec2[MAX_POLY_VERTEX_COUNT];
    public Matrix mat = new Matrix();
    public int[] xPoints;
    public int[] yPoints;

    /**
     * Constructs the Poly. Shape and vertices still need to be set.
     *
     * @param body_ Body that this shape belongs to.
     */
    public Poly( Body body_ )
    {
        body = body_;
    }

    /**
     * Computes the mass of the Body with a given density.
     *
     * @param density Double number describing the density. 0 will set mass to
     *                static.
     */
    @Override
    public void computeMass( double density )
    {
        // Calculate centroid and moment of inertia
        Vec2 c = new Vec2(0.0f, 0.0f); // centroid
        double area = 0.0f;
        double I = 0.0f;
        final double k_inv3 = 1.0f / 3.0f;

        for( int i = 0; i < vertexCount; ++i ){
            // Triangle vertices, third vertex implied as (0, 0)
            Vec2 p1 = vertices[i];
            Vec2 p2 = vertices[( i + 1 ) % vertexCount];

            double D = Vec2.cross(p1, p2);
            double triangleArea = 0.5f * D;

            area += triangleArea;

            // Use area to weight the centroid average, not just vertex position
            double weight = triangleArea * k_inv3;
            c.addsi(p1, weight);
            c.addsi(p2, weight);

            double intx2 = p1.x * p1.x + p2.x * p1.x + p2.x * p2.x;
            double inty2 = p1.y * p1.y + p2.y * p1.y + p2.y * p2.y;
            I += ( 0.25f * k_inv3 * D ) * ( intx2 + inty2 );
        }

        c.muli(1.0f / area);

        // Translate vertices to centroid (make the centroid (0, 0)
        // for the polygon in model space)
        // Not really necessary, but I like doing this anyway
        for( int i = 0; i < vertexCount; ++i ){
            vertices[i].subi(c);
        }

        body.mass = density * area;
        body.inv_mass = ( body.mass != 0.0f ) ? 1.0f / body.mass : 0.0f;
        body.inertia = I * density;
        body.inv_inertia = ( body.inertia != 0.0f ) ? 1.0f / body.inertia : 0.0f;
    }

    /**
     * Overrides the abstract ObjShape method. Does nothing.
     *
     * @param radians Double, radian position of rotation.
     */
    @Override
    public void setOrient( double radians )
    {
        mat.set(radians);
    }

    /**
     * Overrides the abstract ObjShape method. Does nothing.
     *
     * @param mat_ Matrix describing rotation.
     */
    @Override
    public void setOrient( Matrix mat_ )
    {
        mat.set(mat_);
    }

    /**
     * Returns the name of this shape.
     *
     * @return String name of this shape: Polygon.
     */
    @Override
    public String getType()
    {
        return "Polygon";
    }

    /**
     * Sets the vertices of this Polygon to a square, given the width and
     * height.
     *
     * @param hw Double. Half width.
     * @param hh Double. Half height.
     */
    public void setBox( double hw, double hh )
    {
        vertexCount = 4;
        vertices[0] = new Vec2(-hw, -hh);
        vertices[1] = new Vec2(hw, -hh);
        vertices[2] = new Vec2(hw, hh);
        vertices[3] = new Vec2(-hw, hh);
        normals[0] = new Vec2(0.0d, -1.0d);
        normals[1] = new Vec2(1.0d, 0.0d);
        normals[2] = new Vec2(0.0d, 1.0d);
        normals[3] = new Vec2(-1.0d, 0.0d);
        xPoints = new int[vertexCount];
        yPoints = new int[vertexCount];
        for( int i = 0; i < vertexCount; i++ ){
            xPoints[i] = (int) vertices[i].x;
            yPoints[i] = (int) vertices[i].y;
        }
    }

    /**
     * Sets the vertices of this Polygon given a list of vectors.
     *
     * @param verts Vec2 arguments for the vertices of this Polygon.
     */
    public void set( Vec2... verts )
    {
        // Find the right most point on the hull
        int rightMost = 0;
        double highestXCoord = verts[0].x;
        for( int i = 1; i < verts.length; ++i ){
            double x = verts[i].x;

            if( x > highestXCoord ){
                highestXCoord = x;
                rightMost = i;
            } // If matching x then take farthest negative y
            else if( x == highestXCoord ){
                if( verts[i].y < verts[rightMost].y ){
                    rightMost = i;
                }
            }
        }

        int[] hull = new int[MAX_POLY_VERTEX_COUNT];
        int outCount = 0;
        int indexHull = rightMost;

        for( ;; ){
            hull[outCount] = indexHull;

            // Search for next index that wraps around the hull
            // by computing cross products to find the most counter-clockwise
            // vertex in the set, given the previos hull index
            int nextHullIndex = 0;
            for( int i = 1; i < verts.length; ++i ){
                // Skip if same coordinate as we need three unique
                // points in the set to perform a cross product
                if( nextHullIndex == indexHull ){
                    nextHullIndex = i;
                    continue;
                }

                // Cross every set of three unique vertices
                // Record each counter clockwise third vertex and add
                // to the output hull
                Vec2 e1 = verts[nextHullIndex].sub(verts[hull[outCount]]);
                Vec2 e2 = verts[i].sub(verts[hull[outCount]]);
                double c = Vec2.cross(e1, e2);
                if( c < 0.0f ){
                    nextHullIndex = i;
                }

                // Cross product is zero then e vectors are on same line
                // therefore want to record vertex farthest along that line
                if( c == 0.0f && e2.lengthSq() > e1.lengthSq() ){
                    nextHullIndex = i;
                }
            }

            ++outCount;
            indexHull = nextHullIndex;

            // Conclude algorithm upon wrap-around
            if( nextHullIndex == rightMost ){
                vertexCount = outCount;
                break;
            }
        }

        // Copy vertices into shape's vertices
        for( int i = 0; i < vertexCount; ++i ){
            vertices[i] = new Vec2(verts[hull[i]]);
        }

        // Compute face normals
        for( int i = 0; i < vertexCount; ++i ){
            Vec2 face = vertices[( i + 1 ) % vertexCount].sub(vertices[i]);

            // Calculate normal with 2D cross product between vector and scalar
            normals[i] = new Vec2(face.y, -face.x);
            normals[i].normalize();
        }
        xPoints = new int[vertexCount];
        yPoints = new int[vertexCount];
        for( int i = 0; i < vertexCount; i++ ){
            xPoints[i] = (int) vertices[i].x;
            yPoints[i] = (int) vertices[i].y;
        }
    }

    /**
     * Gets the extreme point along a direction within a polygon.
     *
     * @param dir Vec2 direction.
     *
     * @return Vector of the support point.
     */
    public Vec2 getSupport( Vec2 dir )
    {
        double bestProjection = -Float.MAX_VALUE;
        Vec2 bestVertex = null;

        for( int i = 0; i < vertexCount; ++i ){
            Vec2 v = vertices[i];
            double projection = Vec2.dot(v, dir);

            if( projection > bestProjection ){
                bestVertex = v;
                bestProjection = projection;
            }
        }

        return bestVertex;
    }

    /**
     * Sets the two int[] arrays of xPoints and yPoints of the vertices; against
     * the screen.
     * Translates the vertices of this polygon onto the world screen.
     *
     * These points can be accessed from the two int arrays:
     * xPoints
     * yPoints
     */
    public void translatePointsToWorld()
    {
        for( int i = 0; i < vertexCount; i++ ){
            xPoints[i] = (int) mat.mul(vertices[i]).addi(body.position).x;
            yPoints[i] = (int) mat.mul(vertices[i]).addi(body.position).y;
        }
    }
}
