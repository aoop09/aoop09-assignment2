package physXgine;

/**
 *
 * @author Mitchell
 */
public class Matrix
{

    public double m00, m01;
    public double m10, m11;

    public Matrix()
    {
    }

    public Matrix( double radians )
    {
        set(radians);
    }

    public Matrix( double a, double b, double c, double d )
    {
        set(a, b, c, d);
    }

    public double getRadians()
    {
        return Math.atan2(-m01, m00);
    }

    /**
     * Sets this matrix to a rotation matrix with the given radians.
     */
    public void set( double radians )
    {
        double c = (double) StrictMath.cos(radians);
        double s = (double) StrictMath.sin(radians);

        m00 = c;
        m01 = -s;
        m10 = s;
        m11 = c;
    }

    /**
     * Sets the values of this matrix.
     */
    public void set( double a, double b, double c, double d )
    {
        m00 = a;
        m01 = b;
        m10 = c;
        m11 = d;
    }

    /**
     * Sets this matrix to have the same values as the given matrix.
     */
    public void set( Matrix m )
    {
        m00 = m.m00;
        m01 = m.m01;
        m10 = m.m10;
        m11 = m.m11;
    }

    /**
     * Sets the values of this matrix to their absolute value.
     */
    public void absi()
    {
        abs(this);
    }

    /**
     * Returns a new matrix that is the absolute value of this matrix.
     */
    public Matrix abs()
    {
        return abs(new Matrix());
    }

    /**
     * Sets out to the absolute value of this matrix.
     */
    public Matrix abs( Matrix out )
    {
        out.m00 = StrictMath.abs(m00);
        out.m01 = StrictMath.abs(m01);
        out.m10 = StrictMath.abs(m10);
        out.m11 = StrictMath.abs(m11);
        return out;
    }

    /**
     * Sets out to the x-axis (1st column) of this matrix.
     */
    public Vec2 getAxisX( Vec2 out )
    {
        out.x = m00;
        out.y = m10;
        return out;
    }

    /**
     * Returns a new vector that is the x-axis (1st column) of this matrix.
     */
    public Vec2 getAxisX()
    {
        return getAxisX(new Vec2());
    }

    /**
     * Sets out to the y-axis (2nd column) of this matrix.
     */
    public Vec2 getAxisY( Vec2 out )
    {
        out.x = m01;
        out.y = m11;
        return out;
    }

    /**
     * Returns a new vector that is the y-axis (2nd column) of this matrix.
     */
    public Vec2 getAxisY()
    {
        return getAxisY(new Vec2());
    }

    /**
     * Sets the matrix to it's transpose.
     */
    public void transposei()
    {
        double t = m01;
        m01 = m10;
        m10 = t;
    }

    /**
     * Sets out to the transpose of this matrix.
     */
    public Matrix transpose( Matrix out )
    {
        out.m00 = m00;
        out.m01 = m10;
        out.m10 = m01;
        out.m11 = m11;
        return out;
    }

    /**
     * Returns a new matrix that is the transpose of this matrix.
     */
    public Matrix transpose()
    {
        return transpose(new Matrix());
    }

    /**
     * Transforms v by this matrix.
     */
    public Vec2 muli( Vec2 v )
    {
        return mul(v.x, v.y, v);
    }

    /**
     * Sets out to the transformation of v by this matrix.
     */
    public Vec2 mul( Vec2 v, Vec2 out )
    {
        return mul(v.x, v.y, out);
    }

    /**
     * Returns a new vector that is the transformation of v by this matrix.
     */
    public Vec2 mul( Vec2 v )
    {
        return mul(v.x, v.y, new Vec2());
    }

    /**
     * Sets out the to transformation of {x,y} by this matrix.
     */
    public Vec2 mul( double x, double y, Vec2 out )
    {
        out.x = m00 * x + m01 * y;
        out.y = m10 * x + m11 * y;
        return out;
    }

    /**
     * Multiplies this matrix by x.
     */
    public void muli( Matrix x )
    {
        set(
                m00 * x.m00 + m01 * x.m10,
                m00 * x.m01 + m01 * x.m11,
                m10 * x.m00 + m11 * x.m10,
                m10 * x.m01 + m11 * x.m11);
    }

    /**
     * Sets out to the multiplication of this matrix and x.
     */
    public Matrix mul( Matrix x, Matrix out )
    {
        out.m00 = m00 * x.m00 + m01 * x.m10;
        out.m01 = m00 * x.m01 + m01 * x.m11;
        out.m10 = m10 * x.m00 + m11 * x.m10;
        out.m11 = m10 * x.m01 + m11 * x.m11;
        return out;
    }

    /**
     * Returns a new matrix that is the multiplication of this and x.
     */
    public Matrix mul( Matrix x )
    {
        return mul(x, new Matrix());
    }

    public String toString()
    {
        return "[" + Double.toString(m00) + ", " + Double.toString(m01) + ", " + Double.toString(
                m10) + ", " + Double.toString(m11) + "]";
    }
}
