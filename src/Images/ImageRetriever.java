/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Images;

import java.awt.image.BufferedImage;

/**
 *
 * @author warrm1
 */
public interface ImageRetriever
{
    public BufferedImage getImage();
}
