package Images;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;

/**
 * Class used as a retriever for Images in Images package.
 *
 * @author warrm1
 */
public class ImageHolder
        implements ImageRetriever
{
    private String filename;

    /**
     * Selects image
     *
     * @param name Name of selected image.
     */
    public ImageHolder( String name )
    {
        filename = name;
    }

    /**
     * Will return the selected buffered image. If no image can be found it will
     * return the "TubeFloor.png" image.
     *
     * @return A Buffered Image
     */
    @Override
    public BufferedImage getImage()
    {
        try{
            return ImageIO.read(getClass().getResource(filename));
        }catch( IOException e ){
        }
        try{
            return ImageIO.read(getClass().getResource("TubeFloor"));
        }catch( IOException e ){
        }
        return null;
    }
}
