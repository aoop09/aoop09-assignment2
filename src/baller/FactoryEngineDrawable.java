package baller;

import java.awt.Graphics2D;

/**
 * In order for an item to be drawn on the FactoryEngine, it needs to implement
 * this interface.
 *
 * @author warrm1
 */
public interface FactoryEngineDrawable
{

    /**
     * The method that lets an Object be drawn on screen by the FactoryEngine.
     *
     * @param g Graphics2D canvas on which the Object will be drawn.
     */
    public void drawOnGraphic( Graphics2D g );
}
