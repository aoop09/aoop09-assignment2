package baller;

import physXgine.Poly;
import java.awt.Graphics2D;
import physXgine.Matrix;

/**
 * Square class extends FactoryItem and Body. Sets up default values of Body to
 * that of a Square type. Also uses a Poly shape.
 *
 * @author warrm1
 */
public class Square
        extends FactoryItem
{
    public Poly shape;

    /**
     * Constructs a Square at the position on screen given.
     *
     * @param x      Double X position on screen of top left corner.
     * @param y      Double Y position on screen of top left corner.
     * @param width  Double width of the square.
     * @param height Double height of the square.
     */
    public Square( double x, double y, double width, double height )
    {
        //Sets position from top left to center of square.
        super(x + ( width / 2 ), y + ( height / 2 ));
        density = 0.8d;
        restitution = 0.9d;

        shape = new Poly(this);
        shape.setBox(width / 2, height / 2);
        shape.computeMass(density);
        orient = 0;
        shape.setOrient(0);
    }

    /**
     * Overrides Body.getShape(). Returns the shape for this object.
     *
     * @return Poly the shape used by this Body.
     */
    @Override
    public Poly getShape()
    {
        return shape;
    }

    /**
     * Changes orientation of this classes Shape.
     *
     * @param radians Double value between -PI to PI.
     */
    @Override
    public void setOrient( double radians )
    {
        orient = radians;
        shape.setOrient(radians);
    }

    /**
     * Changes orientation of this classes Shape.
     *
     * @param mat Matrix class holding orientation information.
     */
    @Override
    public void setOrient( Matrix mat )
    {
        setOrient(mat.getRadians());
    }

    /**
     * Will rotate and draw the Square on the Graphic given.
     *
     * @param g Graphics2D the canvas on which to be drawn.
     */
    @Override
    public void drawOnGraphic( Graphics2D g )
    {
        g.setColor(colour);
        shape.translatePointsToWorld();
        g.fillPolygon(shape.xPoints, shape.yPoints, shape.vertexCount);
    }
}
