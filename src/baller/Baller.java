package baller;

import java.awt.EventQueue;
import javax.swing.JApplet;
import javax.swing.JFrame;

/**
 * Holds and creates a Baller game scene.
 *
 * @author warrm1
 */
public class Baller
        extends JApplet
{

    /**
     * @param args the command line arguments
     */
    public static void main( String[] args )
    {
        EventQueue.invokeLater(() -> {
            JFrame frame1 = new MainFrame();
            frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame1.setVisible(true);
        });
    }
}
