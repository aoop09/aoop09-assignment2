package baller;

import physXgine.*;
import java.awt.Graphics2D;

/**
 * PolyBox class extends FactoryItem and Body. Sets up default values of Body to
 * that of a PolyBox type. Also uses a Poly shape.
 *
 * @author warrm1
 */
public class PolyBox
        extends FactoryItem
{
    public Poly shape;

    /**
     * Constructs a PolyBox at position given, with a set of vector vertices
     * describing the Polygons shape.
     *
     * @param x        Double X position on screen.
     * @param y        Double Y position on screen.
     * @param density_ Double value of the density for this Body. 0 will make
     *                 this static.
     * @param verts    Vec2 array of vectors describing the vertices of this
     *                 Bodys Poly shape around its position.
     */
    public PolyBox( double x, double y, double density_, Vec2... verts )
    {
        super(x, y);
        density = density_;

        shape = new Poly(this);
        shape.set(verts);
        if( density > 0 ){
            shape.computeMass(density);
        }else{
            setStatic();
        }
        setOrient(0);
    }

    /**
     * Overrides Body.getShape(). Returns the shape for this object.
     *
     * @return Poly the shape used by this Body.
     */
    @Override
    public Poly getShape()
    {
        return shape;
    }

    /**
     * Changes orientation of this classes Shape.
     *
     * @param radians Double value between -PI to PI.
     */
    @Override
    public void setOrient( double radians )
    {
        orient = radians;
        shape.setOrient(radians);
    }

    /**
     * Changes orientation of this classes Shape.
     *
     * @param mat Matrix class holding orientation information.
     */
    @Override
    public void setOrient( Matrix mat )
    {
        setOrient(mat.getRadians());
    }

    /**
     * Will rotate and draw the Square on the Graphic given.
     *
     * @param g Graphics2D the canvas on which to be drawn.
     */
    @Override
    public void drawOnGraphic( Graphics2D g )
    {
        g.setColor(colour);
        shape.translatePointsToWorld();
        g.fillPolygon(shape.xPoints, shape.yPoints, shape.vertexCount);
    }
}
