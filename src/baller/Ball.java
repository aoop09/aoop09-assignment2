package baller;

import physXgine.Circle;
import java.awt.Graphics2D;
import physXgine.Matrix;

/**
 * Ball class extends FactoryItem and Body. Sets up default values of Body to
 * that of a Ball type. Also uses a circle shape.
 *
 * @author warrm1
 */
public class Ball
        extends FactoryItem
{
    private static final double RADIUS = 7;
    public Circle shape;

    /**
     * Constructs a Ball at the position on screen given.
     *
     * @param x Double X position on screen.
     * @param y Double Y position on screen.
     */
    public Ball( double x, double y )
    {
        super(x, y);

        density = 1.0d;

        restitution = 0.7d;
        staticFriction = 0.1d;
        dynamicFriction = 0.0d;

        shape = new Circle(RADIUS, this);
        shape.computeMass(density);
    }

    /**
     * Overrides Body.getShape(). Returns the shape for this object.
     *
     * @return Circle the shape used by this Body.
     */
    @Override
    public Circle getShape()
    {
        return shape;
    }

    /**
     * Changes orientation of this classes Shape.
     *
     * @param radians Double value between -PI to PI.
     */
    @Override
    public void setOrient( double radians )
    {
        orient = radians;
        shape.setOrient(radians);
    }

    /**
     * Changes orientation of this classes Shape.
     *
     * @param mat Matrix class holding orientation information.
     */
    @Override
    public void setOrient( Matrix mat )
    {
        setOrient(mat.getRadians());
    }

    /**
     * Will draw the Ball on the Graphic given.
     *
     * @param g Graphics2D the canvas on which to be drawn.
     */
    @Override
    public void drawOnGraphic( Graphics2D g )
    {
        g.setColor(colour);
        g.fillOval((int) position.x - (int) shape.radius,
                   (int) position.y - (int) shape.radius, (int) RADIUS * 2,
                   (int) RADIUS * 2);
    }
}
