package baller;

import machines.*;
import physXgine.*;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.*;
import java.util.*;
import java.util.Timer;

/**
 * Frame that joins a FactoryEngine and a ButtonPanel together to make a user
 * oriented scene.
 *
 * @author warrm1
 */
public class MainFrame
        extends JFrame
        implements MouseListener,
                   GUISelection
{
    public FactoryEngine comp;
    public static final int DELAY = 3;
    public Thread sceneCollision;
    public Thread sceneTimer;
    public static String selected = "";
    public static boolean mousePressed;
    public ArrayList<Thread> sceneThreads = new ArrayList();
    public Timer timer;
    public Machine selectedMachine;
    public static final Color red = Color.getHSBColor(0.96f, 0.9f, 0.9f);
    public static final Color green = Color.getHSBColor(0.3f, 0.9f, 0.7f);
    public static final Color blue = Color.getHSBColor(0.6f, 0.9f, 0.9f);

    public static double globalDT;

    /**
     * Constructs a new MainFrame.
     */
    public MainFrame()
    {
        setTitle("Bounce");

        comp = new FactoryEngine(this);
        comp.setBorder(BorderFactory.createLineBorder(Color.black));
        comp.setLayout(null);
        //creates mouse listening capabilities for Engine
        comp.addMouseListener(this);

        /**
         * @author Nik
         */
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(0, 2));
        buttonPanel.setBorder(BorderFactory.createLineBorder(Color.red));
        buttonPanel.setBounds(0, 10, 400, 340);
        //buttonPanel.setMaximumSize(new Dimension(500,500));

        addButton(buttonPanel, 80, "Ball Dropper",
                  event -> select("BallDropper"));
        addButton(buttonPanel, 40, "Square Dropper", event -> select(
                  "SquareDropper"));
        addButton(buttonPanel, 40, "Floor", event -> select("TubeFloor"));
        addButton(buttonPanel, 40, "Wall", event -> select("TubeWall"));
        addButton(buttonPanel, 40, "Conveyor", event -> select("Conveyor"));
        addButton(buttonPanel, 40, "Tube Corner", event -> select("TubeCorner"));
        addButton(buttonPanel, 40, "Painter", event -> select("Painter"));
        addButton(buttonPanel, 40, "Squisher", event -> select("Squisher"));
        addButton(buttonPanel, 40, "Delete", event -> select("Delete"));
        addButton(buttonPanel, 40, "Close", event -> System.exit(0));
        addButton(buttonPanel, 40, "Clear", event -> clear());
        //add(buttonPanel, BorderLayout.SOUTH);
        add(buttonPanel, BorderLayout.EAST);
        add(comp, BorderLayout.CENTER);

        pack();

        //Clears the scene which also restarts it and sets defaults.
        clear();
    }

    /**
     * Adds a new button to a container.
     *
     * @param c        Container to add a button to.
     * @param size     int sets width of button.
     * @param title    String the buttons text.
     * @param listener ActionListener method to run on click.
     */
    public void addButton( Container c, int size, String title, ActionListener listener )
    {
        JButton button = new JButton(title);
        button.setPreferredSize(new Dimension(size, 40));
        button.setBorder(BorderFactory.createLineBorder(Color.green));
        c.add(button);
        button.addActionListener(listener);
    }

    /**
     * Implements GUISelection interface. Sets the selected machine.
     *
     * @param selectString String of selected item.
     */
    public void select( String selectString )
    {
        //Allows to unselect a Machine.
        if( selected.equals(selectString) ){
            selected = "";
        }else{
            selected = selectString;
        }
        //The selectedMachine will be used to create the ghost image on screen.
        switch( selected ){
            case "BallDropper":
                selectedMachine = new BallDropper(new Vec2(0, 0), comp);
                break;
            case "SquareDropper":
                selectedMachine = new SquareDropper(new Vec2(0, 0), comp);
                break;
            case "TubeFloor":
                selectedMachine = new TubeFloor(new Vec2(0, 0));
                break;
            case "TubeWall":
                selectedMachine = new TubeWall(new Vec2(0, 0));
                break;
            case "TubeCorner":
                selectedMachine = new TubeCorner(new Vec2(0, 0));
                break;
            case "Conveyor":
                selectedMachine = new Conveyor(new Vec2(0, 0));
                break;
            case "Painter":
                selectedMachine = new Painter(new Vec2(0, 0));
                break;
//            case "Squisher":
//                selectedMachine = new Squisher(new Vec2(0,0));
//                break;
            default:
                selectedMachine = null;
                break;
        }
    }

    /**
     * Will clear the scene and reset defaults.
     */
    public void clear()
    {
        comp.clearItems();
        comp.clearMachines();
        createScene();
        ImpulseMath.GRAVITY = new Vec2(0, 10);
        comp.repaint();
    }

    /**
     * Recreates scene threads.
     */
    public void createScene()
    {
        comp.stopThreadPool();
        comp.startThreadPool();
        for( Thread t : sceneThreads ){
            t.interrupt();
        }
        sceneThreads.clear();

        Runnable sceneTimeChanger = () -> {
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask()
            {
                public void run()
                {
                    globalDT += 0.01d;
                    if( globalDT > 100 ){
                        globalDT = 0;
                    }
                }
            }, 500, 10);
        };
        sceneTimer = new Thread(sceneTimeChanger);
        sceneTimer.start();
        sceneThreads.add(sceneTimer);

        Thread t = new Thread(Conveyor.getRun());
        sceneThreads.add(t);
        t.start();
    }

    /**
     * Will place a Machine in the scene.
     *
     * @param m Machine to be placed.
     */
    public void placeMachine( Machine m )
    {
        comp.addMachine(m);
    }

    /**
     * Will run a thread allowing the rotation of a Machine on the FactoryEngine
     * screen.
     *
     * @param m Machine to be rotated.
     */
    public void rotateMachine( Machine m )
    {
        Runnable r = () -> {
            Graphics g = comp.getGraphics();
            try{
                Vec2 mouseVector = new Vec2(comp.getMousePosition().getX(),
                                            comp.getMousePosition().getY());
                Vec2 rotateVector = new Vec2();
                double angle;
                double quaterPI = ( ImpulseMath.PI / 4 );
                while( mousePressed && !Thread.currentThread().isInterrupted() ){
                    angle = m.getPos().sub(mouseVector).getAngle();

                    //Rounds angles to the nearest 90 degrees.
                    if( angle > -quaterPI * 3 && angle < -quaterPI ){
                        //Down
                        angle = ImpulseMath.PI;
                        rotateVector.set(m.getPos().add(new Vec2(0, 40)));
                    }else if( angle > -quaterPI && angle < quaterPI ){
                        //Left
                        angle = -quaterPI * 2;
                        rotateVector.set(m.getPos().add(new Vec2(-40, 0)));
                    }else if( angle > quaterPI && angle < quaterPI * 3 ){
                        //Up
                        angle = 0;
                        rotateVector.set(m.getPos().add(new Vec2(0, -40)));
                    }else{
                        //Right
                        angle = ( ImpulseMath.PI / 2 );
                        rotateVector.set(m.getPos().add(new Vec2(40, 0)));
                    }

                    m.setOrient(angle);
                    try{
                        mouseVector.set(comp.getMousePosition().getX(),
                                        comp.getMousePosition().getY());
                        comp.paintLine(g, m.getPos(), rotateVector);
                        comp.repaint();
                    }catch( NullPointerException e ){
                    }
                }
                comp.repaint();
                //Catches the mouse out of bounds.
            }catch( NullPointerException e ){
            }
        };
        Thread t = new Thread(r);
        t.start();
    }

    /**
     * Draws a ghost Machine on the canvas that shows what Machine will be
     * placed. Implements FactoryEngineDrawable.
     *
     * @param g Graphics2D canvas to be drawn on.
     */
    public void drawOnGraphic( Graphics2D g )
    {
        if( selectedMachine != null && !mousePressed ){
            //Basically just does a modified version of the rotate machine method.
            double angle;
            double quaterPI = ( ImpulseMath.PI / 4 );
            try{
                selectedMachine.setPos(new Vec2(
                        ( ( (int) comp.getMousePosition().getX() / 40 ) * 40 ) + 20,
                        ( ( (int) comp.getMousePosition().getY() / 40 ) * 40 ) + 20));
                angle = selectedMachine.getPos().sub(new Vec2(
                        comp.getMousePosition().getX(),
                        comp.getMousePosition().getY())).getAngle();

                if( angle > -quaterPI * 3 && angle < -quaterPI ){
                    //Down
                    angle = ImpulseMath.PI;
                }else if( angle > -quaterPI && angle < quaterPI ){
                    //Left
                    angle = -quaterPI * 2;
                }else if( angle > quaterPI && angle < quaterPI * 3 ){
                    //Up
                    angle = 0;
                }else{
                    //Right
                    angle = ( ImpulseMath.PI / 2 );
                }

                selectedMachine.setOrient(angle);
                //Sets the image to be half transparent.
                g.setComposite(AlphaComposite.getInstance(
                        AlphaComposite.SRC_OVER, 0.3f));
                selectedMachine.drawOnGraphic(g);
            }catch( NullPointerException e ){
            }finally{
                g.setComposite(AlphaComposite.getInstance(
                        AlphaComposite.SRC_OVER, 1f));
            }
        }
    }

    /*
     * This is where mouselistener starts
     * This class needs to IMPLEMENT MouseListener
     * These classes underneath are required
     */
    /**
     * Method run when mouse is pressed. Will try to place a selected machine.
     *
     * @param event Event of mouse being clicked.
     */
    public void mousePressed( MouseEvent event )
    {
        mousePressed = true;
        switch( selected ){
            case "Delete":
                comp.removeMachine(new Vec2(event.getX(), event.getY()));
                break;
            case "BallDropper":
                BallDropper machineBD = new BallDropper(new Vec2(event.getX(),
                                                                 event.getY()),
                                                        comp);
                placeMachine(machineBD);
                break;
            case "SquareDropper":
                SquareDropper machineSD = new SquareDropper(new Vec2(
                        event.getX(), event.getY()), comp);
                placeMachine(machineSD);
                break;
            case "TubeFloor":
                TubeFloor machineTF = new TubeFloor(new Vec2(event.getX(),
                                                             event.getY()));
                placeMachine(machineTF);
                rotateMachine(machineTF);
                break;
            case "TubeWall":
                TubeWall machineTW = new TubeWall(new Vec2(event.getX(),
                                                           event.getY()));
                placeMachine(machineTW);
                rotateMachine(machineTW);
                break;
            case "TubeCorner":
                TubeCorner machineTC = new TubeCorner(new Vec2(event.getX(),
                                                               event.getY()));
                placeMachine(machineTC);
                rotateMachine(machineTC);
                break;
            case "Conveyor":
                Conveyor machineC = new Conveyor(new Vec2(event.getX(),
                                                          event.getY()));
                placeMachine(machineC);
                rotateMachine(machineC);
                break;
            case "Painter":
                Painter machineP = new Painter(new Vec2(event.getX(),
                                                        event.getY()));
                placeMachine(machineP);
                rotateMachine(machineP);
                Thread t = new Thread(machineP.getRun(machineP, comp));
                machineP.thread = t;
                sceneThreads.add(t);
                t.start();
                break;
//            case "Squisher":
//                Squisher machineS = new Squisher(new Vec2(event.getX(), event.getY()));
//                placeMachine(machineS);
//                rotateMachine(machineS);
//                Thread t = new Thread(machineS.getRun(machineS, comp));
//                machineP.thread = t;
//                sceneThreads.add(t);
//                t.start();
//                break;
            default:
                break;
        }
    }

    /**
     * Method run when mouse is clicked. Does nothing
     *
     * @param event Event of mouse being clicked.
     */
    public void mouseClicked( MouseEvent event )
    {
    }

    /**
     * Method run when mouse enters the screen. Does nothing
     *
     * @param event Event of mouse entering.
     */
    public void mouseEntered( MouseEvent event )
    {
    }

    /**
     * Method run when mouse exits the screen. Does nothing
     *
     * @param event Event of mouse exiting.
     */
    public void mouseExited( MouseEvent event )
    {
    }

    /**
     * Method run when mouse is released. Will set mousePressed to false.
     *
     * @param event Event of mouse being released.
     */
    public void mouseReleased( MouseEvent event )
    {
        mousePressed = false;
    }
}
