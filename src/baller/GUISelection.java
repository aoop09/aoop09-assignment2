package baller;

/**
 * In order to allow a selection from a button panel, this interface is
 * required.
 *
 * @author warrm1
 */
public interface GUISelection
{

    /**
     * The method that buttons from a button panel will try to access.
     *
     * @param selectedString String the text describing what was selected.
     */
    public void select( String selectedString );
}
