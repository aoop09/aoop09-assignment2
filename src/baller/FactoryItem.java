package baller;

import static baller.MainFrame.DELAY;
import java.awt.Graphics2D;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import physXgine.*;

/**
 * Base class extending a Body and used to keep both Balls and Squares within a
 * FactoryEngine.
 *
 * @author Mitchell
 */
public abstract class FactoryItem
        extends Body
        implements FactoryEngineDrawable
{
    public FactoryEngine enviroment;
    public Lock factoryItem_Lock = new ReentrantLock();

    /**
     * Constructs a new FactoryItem at a position.
     *
     * @param x Double X position on screen.
     * @param y Double Y position on screen.
     */
    public FactoryItem( double x, double y )
    {
        super();
        position = new Vec2(x, y);
        objDT = MainFrame.globalDT;
    }

    /**
     * Will draw this Body on a FactoryEngine.
     *
     * @param g Graphics2D the canvas on which to be drawn.
     */
    @Override
    abstract public void drawOnGraphic( Graphics2D g );

    /**
     * Runs this factory item within the FactoryEngine. Will move the item and
     * check for collision. Does not repeat.
     */
    public void run()
    {
        try{
            if( MainFrame.globalDT - objDT > ( 1d / 60d ) || MainFrame.globalDT < objDT ){
                objDT = MainFrame.globalDT - ( 1d / 120d );
            }
            if( mass == 0 ){
                return;
            }
            Vec2 ghostPos = new Vec2();
            move(( MainFrame.globalDT - objDT ) * 5);
            if( !position.withinBounds(0, 0, enviroment.getBounds().width,
                                       enviroment.getBounds().height) ){
                enviroment.removeItem(this);
                return;
            }
            for( int i = -20; i < 21; i += 40 ){
                ghostPos.x = position.x + i;
                for( int n = -20; n < 21; n += 40 ){
                    ghostPos.y = position.y + n;
                    if( enviroment.getMachine(ghostPos) != null ){
                        enviroment.getMachine(ghostPos).resolveCollision(this);
                    }
                }
            }
            Collision.resolveCollision(this, enviroment.getItems());
            force.set(0, 0);
            torque = 0;
            objDT = MainFrame.globalDT;
            Thread.sleep(DELAY);
        }catch( InterruptedException e ){
        }
    }
}
