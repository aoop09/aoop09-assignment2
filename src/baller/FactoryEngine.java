package baller;

import machines.*;
import physXgine.Vec2;
import physXgine.Body;
import javax.swing.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Dimension;
import java.util.*;
import java.util.concurrent.locks.*;

/**
 * Environment that extends JPanel and allows Bodys to be drawn and run on
 * screen like a Physics engine window.
 *
 * @author Mitchell
 */
public class FactoryEngine
        extends JPanel
{
    //Having a mainframe as a parent allows us to use it as a drawable.
    public MainFrame parent;
    //Sets max number of items on screen allowed.
    private final int maxItems = 350;
    private final int DEFAULT_WIDTH = 800;
    private final int DEFAULT_HEIGHT = 900;

    public List<Body> syncItems = Collections.synchronizedList(
            new ArrayList<Body>());

    public Machine[][] mitems;
    public Lock mitems_Lock = new ReentrantLock();

    public WorkQueue worker = new WorkQueue();

    /**
     * Constructs a FactoryEngine with the parent MainFrame.
     *
     * @param parent_ MainFrame Frame on which FactoryEngine is to be used.
     */
    public FactoryEngine( MainFrame parent_ )
    {
        setBackground(Color.WHITE);
        parent = parent_;
    }

    /**
     * Removes a FactoryItem from the scene.
     *
     * @param b Body to be removed.
     */
    public void removeItem( Body b )
    {
        syncItems.remove(b);
        worker.deleteFromQueue();
    }

    /**
     * Adds a FactoryItem to the scene.
     *
     * @param b Body to be added.
     */
    public void addItem( Body b )
    {
        if( b == null || syncItems.size() >= maxItems ){
            return;
        }
        syncItems.add(b);
        worker.addToQueue();
    }

    /**
     * Deletes all items from the scene.
     */
    public void clearItems()
    {
        syncItems.clear();
        worker.clearQueue();
    }

    /**
     * Removes a Machine from the scene at a given point.
     *
     * @param pos Vec2 position on screen to set as null.
     */
    public void removeMachine( Vec2 pos )
    {
        mitems_Lock.lock();
        try{
            if( mitems[(int) pos.x / 40][(int) pos.y / 40] != null ){
                if( mitems[(int) pos.x / 40][(int) pos.y / 40].thread != null ){
                    mitems[(int) pos.x / 40][(int) pos.y / 40].thread.interrupt();
                }
            }
            mitems[(int) pos.x / 40][(int) pos.y / 40] = null;
            repaint();
        }catch( ArrayIndexOutOfBoundsException e ){
        }finally{
            mitems_Lock.unlock();
        }
    }

    /**
     * Adds a Machine to the scene. Its uses the position in the Machine
     * already.
     * If a Machine already exists there, it will replace it.
     *
     * @param b Machine to be added.
     */
    public void addMachine( Machine b )
    {
        mitems_Lock.lock();
        //divide by 40 to get to the position of the nearest tile.
        try{
            if( mitems[(int) b.getPos().x / 40][(int) b.getPos().y / 40] != null ){
                if( mitems[(int) b.getPos().x / 40][(int) b.getPos().y / 40].thread != null ){
                    mitems[(int) b.getPos().x / 40][(int) b.getPos().y / 40].thread.interrupt();
                }
            }
            mitems[(int) b.getPos().x / 40][(int) b.getPos().y / 40] = b;
            repaint();
        }catch( ArrayIndexOutOfBoundsException e ){
        }finally{
            mitems_Lock.unlock();
        }
    }

    /**
     * Will delete all Machines from the scene.
     */
    public void clearMachines()
    {
        mitems_Lock.lock();
        try{
            mitems = new Machine[( (int) getBounds().getWidth() ) / 40][( (int) getBounds().getHeight() ) / 40];
        }finally{
            mitems_Lock.unlock();
        }
    }

    /**
     * Will return the Machine at a given tile position.
     *
     * @param v Vec2 position on screen to check for Machine.
     *
     * @return Machine at checked for position. If no Machine there; will return
     *         null.
     */
    public Machine getMachine( Vec2 v )
    {
        try{
            return mitems[(int) v.x / 40][(int) v.y / 40];
            //In case the position given is beyond the FactoryEngine.
        }catch( ArrayIndexOutOfBoundsException e ){
            return null;
        }
    }

    /**
     * Gets all FactoryItems from scene.
     *
     * @return List the thread safe ArrayList of Bodys.
     */
    public List<Body> getItems()
    {
        return syncItems;
    }

    /**
     * Will paint the scene unto the JPanel.
     *
     * @param g Graphics canvas to be painted onto.
     */
    @Override
    public void paintComponent( Graphics g )
    {
        //Erase background.
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        //Draw MainFrame at lowest layer
        parent.drawOnGraphic(g2);

        //Draw Items at middle layer.
        for( int i = 0; i < syncItems.size(); i++ ){
            try{
                ( (FactoryItem) syncItems.get(i) ).drawOnGraphic(g2);
                //Painting happens so rapidly that if something goes wrong its not a big deal,
                //to have to paint it on a second go through.
            }catch( ConcurrentModificationException | IndexOutOfBoundsException e ){
            }
        }

        //Draw Machines at top layer.
        mitems_Lock.lock();
        try{
            for( int i = 0; i < ( (int) getBounds().getWidth() ) / 40; i++ ){
                for( int n = 0; n < ( (int) getBounds().getHeight() ) / 40; n++ ){
                    if( mitems[i][n] != null ){
                        mitems[i][n].drawOnGraphic(g2);
                    }
                }
            }
        }catch( Exception e ){
        }finally{
            mitems_Lock.unlock();
        }
    }

    /**
     * Draws a line on screen given two Vec2.
     *
     * @param g    Graphics canvas to be drawn on.
     * @param vec1 Vec2 position one.
     * @param vec2 Vec2 position two.
     */
    public void paintLine( Graphics g, Vec2 vec1, Vec2 vec2 )
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.gray);
        g2.drawLine((int) vec1.x, (int) vec1.y, (int) vec2.x, (int) vec2.y);
    }

    /**
     * Gets the size wanted by this object.
     *
     * @return Dimension of preferred width and height.
     */
    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Stops the scene from moving FactoryItems and Machines.
     */
    public void stopThreadPool()
    {
        try{
            worker.clearQueue();
        }catch( Exception e ){
        }
    }

    /**
     * Restarts the scene
     */
    public void startThreadPool()
    {
        //Interrupts all threads first.
        stopThreadPool();
        worker = new WorkQueue();
    }

    /**
     * Class that pools threads together to work on moving all FactoryItems and
     * Machines.
     *
     * @Mitchell
     */
    public class WorkQueue
    {
        ArrayList<PoolWorker> threads;
        MachineWorker machineThread;
        ScreenPainter paintThread;

        //Keeps the place in the item list so that no two threads can work on the same item.
        int itemPlace = 0;
        Lock itemPlace_Lock = new ReentrantLock();

        /**
         * Constructs a new WorkQueue.
         */
        public WorkQueue()
        {
            machineThread = new MachineWorker();
            machineThread.start();
            paintThread = new ScreenPainter();
            paintThread.start();
            threads = new ArrayList();
            addToQueue();
        }

        /**
         * Adds threads to the working pool as needed.
         */
        public void addToQueue()
        {
            //Keeps a proper ratio of 3 items to 1 thread.
            while( threads.size() < ( syncItems.size() / 3 ) + 1 ){
                PoolWorker pw = new PoolWorker();
                pw.start();
                threads.add(pw);
            }
        }

        /**
         * Deletes and interrupts threads from the thread pool as needed.
         */
        public void deleteFromQueue()
        {
            //Keeps a proper ratio of 3 items to 1 thread.
            while( threads.size() > ( syncItems.size() / 3 ) + 1 ){
                threads.get(threads.size() - 1).interrupt();
                threads.remove(threads.size() - 1);
            }
        }

        /**
         * Will interrupt and delete all threads from the scene.
         */
        public void clearQueue()
        {
            machineThread.interrupt();
            paintThread.interrupt();
            for( PoolWorker pw : threads ){
                pw.interrupt();
            }
            threads.clear();
        }

        /**
         * Thread that can work as a swarm or pool of threads on its particular
         * focus.
         *
         * @Mitchell
         */
        private class PoolWorker
                extends Thread
        {
            /**
             * The runnable function that this Thread uses.
             */
            public void run()
            {
                //Keeps its own place to decrease time spent getting the next place in line.
                int placing;
                FactoryItem item;
                while( !Thread.currentThread().isInterrupted() ){
                    //As this thread will be running constanly and over the course of multiple
                    //items, if one item returns an error for any reason; the thread will just move on to the next one.
                    try{
                        //Quickly coppies what the place is and then sets it to teh next one.
                        itemPlace_Lock.lock();
                        try{
                            placing = itemPlace;
                            itemPlace++;
                            if( itemPlace > syncItems.size() - 1 ){
                                itemPlace = 0;
                            }
                        }finally{
                            itemPlace_Lock.unlock();
                        }

                        //The work is done with the copied place so as to keep the threads moving along.
                        item = (FactoryItem) syncItems.get(placing);
                        try{
                            item.factoryItem_Lock.lock();
                            try{
                                item.run();
                            }catch( RuntimeException e ){
                            }
                        }finally{
                            item.factoryItem_Lock.unlock();
                        }
                    }catch( Exception e ){
                    }
                }
            }
        }

        /**
         * Extends thread and runs all Machines at regular intervals.
         */
        private class MachineWorker
                extends Thread
        {
            /**
             * The runnable function that this Thread uses.
             */
            public void run()
            {
                //As this thread will be running constanly and over the course of multiple
                //machines, if one item returns an error for any reason; the thread will just move on to the next one.
                try{
                    while( !Thread.currentThread().isInterrupted() ){
                        mitems_Lock.lock();
                        try{
                            for( int i = 0; i < ( (int) getBounds().getWidth() ) / 40; i++ ){
                                for( int n = 0; n < ( (int) getBounds().getHeight() ) / 40; n++ ){
                                    if( mitems[i][n] != null ){
                                        //Running a machine might take a while so best to unlock.
                                        mitems_Lock.unlock();
                                        try{
                                            mitems[i][n].run();
                                            //In case somethign goes wrong with this machine we relock on the way out in a finally block.
                                        }finally{
                                            mitems_Lock.lock();
                                        }
                                    }
                                }
                            }
                        }catch( Exception e ){
                        }finally{
                            mitems_Lock.unlock();
                        }
                        Thread.sleep(1000);
                    }
                }catch( InterruptedException e ){
                }
            }
        }

        /**
         * Extends thread and repaints the scene at regular intervals.
         */
        private class ScreenPainter
                extends Thread
        {
            public void run()
            {
                try{
                    while( !Thread.currentThread().isInterrupted() ){
                        repaint();
                        Thread.sleep(5);
                    }
                }catch( InterruptedException e ){
                }
            }
        }
    }
}
