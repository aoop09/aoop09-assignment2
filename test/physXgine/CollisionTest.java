package physXgine;

import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;
import baller.*;

/**
 *
 * @author warrm1
 */
public class CollisionTest {
    ArrayList<Body> bodies = new ArrayList();
    
    public CollisionTest() {
        for(int i = 0; i < 10; i++){
            bodies.add(new Ball(0, i*40));
        }
    }

    @Test
    public void testGetCollision() {
        //None of the initialized Balls are touching
        //None should be touching the first
        assertTrue(Collision.getCollision(bodies.get(0), bodies).isEmpty());
        //Move the last one next to the first
        bodies.get(9).position.set(0, 10);
        //1 should be touching the first
        assertTrue(Collision.getCollision(bodies.get(0), bodies).size() == 1);
        //Move the last one directly on top of the first
        bodies.get(9).position.set(0, 0);
        bodies.get(0).position.set(0, 0);
        //2 should be touching the first
        assertTrue(Collision.getCollision(bodies.get(0), bodies).size() == 1);
        //Move the last two directly on top of the first
        bodies.get(8).position.set(0, 0);
        bodies.get(9).position.set(0, 0);
        bodies.get(0).position.set(0, 0);
        //2 should be touching the first
        assertTrue(Collision.getCollision(bodies.get(0), bodies).size() == 2);
        //Move the last two off the first
        bodies.get(8).position.set(0, 40);
        bodies.get(9).position.set(0, 40);
        //None should be touching the first
        assertTrue(Collision.getCollision(bodies.get(0), bodies).isEmpty());
    }
    
}
