package baller;

import org.junit.Test;
import static org.junit.Assert.*;
import physXgine.ImpulseMath;

/**
 *
 * @author Mitchell
 */
public class SquareTest
{
    Square instance = new Square(0 - 7, 0 - 7, 14, 14);
    Square instanceCopy = new Square(0 - 7, 0 - 7, 14, 14);
    
    public SquareTest() {
    }

    @Test
    public void testRun() {
        instance.move(0.1);
        //Make sure both have the same outcome
        assertTrue(instance.position.y == instanceCopy.position.addsi(instanceCopy.velocity.addsi(ImpulseMath.GRAVITY, 0.1*0.5f), 0.1).y);
        instanceCopy.velocity.addsi(ImpulseMath.GRAVITY, 0.1*0.5f);
        instance.move(0.34);
        //Make sure both have the same outcome
        assertTrue(instance.position.y == instanceCopy.position.addsi(instanceCopy.velocity.addsi(ImpulseMath.GRAVITY, 0.34*0.5f), 0.34).y);
        instanceCopy.velocity.addsi(ImpulseMath.GRAVITY, 0.34*0.5f);
        instance.move(-0.3);
        //Make sure both can reverse
        assertTrue(instance.position.y == instanceCopy.position.addsi(instanceCopy.velocity.addsi(ImpulseMath.GRAVITY, -0.3*0.5f), -0.3).y);
        instanceCopy.velocity.addsi(ImpulseMath.GRAVITY, -0.3*0.5f);
    }
}
