package baller;

import org.junit.Test;
import static org.junit.Assert.*;
import physXgine.*;

/**
 *
 * @author Mitchell
 */
public class FactoryEngineTest
{
    public FactoryEngine instance;
    
    public FactoryEngineTest()
    {
        MainFrame mframe = new MainFrame();
        instance = new FactoryEngine(mframe);
        instance.clearMachines();
    }

    /**
     * Test of addItem method, of class FactoryEngine.
     */
    @Test
    public void testAddItem()
    {
        Body a = new Ball(0,0);
        instance.addItem(a);
        assertTrue(instance.getItems().size() == 1);
        instance.addItem(a);
        assertTrue(instance.getItems().size() == 2);
        instance.addItem(null);
        assertTrue(instance.getItems().size() == 2);
    }

    /**
     * Test of removeItem method, of class FactoryEngine.
     */
    @Test
    public void testRemoveItem()
    {
        Body a = new Ball(0,0);
        Body b = new Ball(0,0);
        Body c = new Ball(0,0);
        instance.addItem(a);
        instance.addItem(b);
        instance.addItem(c);
        assertTrue(instance.getItems().size() == 3);
        instance.removeItem(a);
        assertTrue(instance.getItems().size() == 2);
        instance.removeItem(instance.getItems().get(1));
        assertTrue(instance.getItems().size() == 1);
        assertTrue(instance.getItems().get(0).equals(b));
    }

    /**
     * Test of clearItems method, of class FactoryEngine.
     */
    @Test
    public void testClearItems()
    {
        Body a = new Ball(0,0);
        instance.addItem(a);
        instance.clearItems();
        assertTrue(instance.getItems().isEmpty());
    }
}
